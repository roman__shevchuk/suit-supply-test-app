﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace Common.Web.PatchHelpers
{
    public static class IChangesExtension
    {
        public static bool HasChanged<T>(this IChanges<T> changes, params Expression<Func<T, object>>[] properties)
            where T : class
        {
            return properties.Any(changes.HasChanged);
        }
    }
}