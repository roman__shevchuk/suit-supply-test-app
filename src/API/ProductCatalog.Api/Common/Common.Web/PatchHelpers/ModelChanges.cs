﻿using Common.Web.Binders;
using Microsoft.AspNetCore.Mvc;

namespace Common.Web.PatchHelpers
{
    [ModelBinder(typeof(PatchJsonWithFilesFormDataModelBinder), Name = ModelBinderConstants.JsonPropertyName)]
    public class Changes<T> : GenericChanges<T>
        where T : class, new()
    {
    }
}