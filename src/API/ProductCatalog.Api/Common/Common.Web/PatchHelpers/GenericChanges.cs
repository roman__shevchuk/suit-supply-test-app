﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Common.Web.PatchHelpers
{
    public class GenericChanges<T> : Changes, IChanges<T>
        where T : class, new()
    {
        private T entity;

        public T Entity
        {
            get
            {
                if (entity == null)
                {
                    this.entity = new T();
                    this.ApplyTo(entity);
                }

                return entity;
            }
        }

        public void ApplyTo(T entityToUpdate)
        {
            foreach (var (property, value) in this.TrackedChanges)
            {
                property.SetValue(entityToUpdate, value);
            }
        }

        public bool HasChanged(Expression<Func<T, object>> func)
        {
            string propertyName = func.GetName();
            return this.TrackedChanges.Any(x => x.Property.Name == propertyName);
        }

        public bool HasChanged<TProperty>(Expression<Func<T, TProperty>> func, TProperty currentValue)
        {
            string propertyName = func.GetName();
            return this.TrackedChanges.Any(x => x.Property.Name == propertyName && x.Value != (object)currentValue);
        }

        public TProperty GetValue<TProperty>(Expression<Func<T, TProperty>> func)
        {
            string propertyName = func.GetName();
            return (TProperty)this.TrackedChanges.First(x => x.Property.Name == propertyName).Value;
        }

        public void AddChange<TProperty>(Expression<Func<T, TProperty>> func, TProperty changedValue)
        {
            string propertyName = func.GetName();
            PropertyInfo propertyInfo = typeof(T).GetProperty(propertyName);
            this.AddChange(propertyInfo, changedValue);
        }
    }
}