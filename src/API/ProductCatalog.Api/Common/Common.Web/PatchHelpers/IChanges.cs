﻿using System;
using System.Linq.Expressions;

namespace Common.Web.PatchHelpers
{
    public interface IChanges<T>
        where T : class
    {
        void ApplyTo(T entityToUpdate);

        bool HasChanged(Expression<Func<T, object>> func);
    }
}