﻿using System;
using System.Linq.Expressions;
using System.Reflection;

namespace Common.Web.PatchHelpers
{
    public class DynamicChange<TTo, TProperty> : IChanges<TTo>
        where TTo : class
    {
        private readonly Action<TProperty> change;
        private readonly PropertyInfo propertyInfo;

        public DynamicChange(Expression<Func<TTo, TProperty>> propertyRetriever, Action<TProperty> change)
        {
            this.change = change;

            string propertyName = propertyRetriever.GetName();
            this.propertyInfo = typeof(TTo).GetProperty(propertyName);
        }

        public void ApplyTo(TTo entityToUpdate)
        {
            var property = (TProperty)this.propertyInfo.GetValue(entityToUpdate);
            this.change(property);
        }

        public bool HasChanged(Expression<Func<TTo, object>> func)
        {
            string propertyName = func.GetName();
            return propertyName == this.propertyInfo.Name;
        }
    }
}