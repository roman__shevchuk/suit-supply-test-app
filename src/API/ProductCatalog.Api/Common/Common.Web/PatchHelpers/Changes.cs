﻿using System.Collections.Generic;
using System.Reflection;

namespace Common.Web.PatchHelpers
{
    public abstract class Changes
    {
        public List<(PropertyInfo Property, object Value)> TrackedChanges { get; } = new List<(PropertyInfo Property, object Value)>();

        public void AddChange(PropertyInfo prop, object value)
        {
            this.TrackedChanges.Add((prop, value));
        }
    }
}
