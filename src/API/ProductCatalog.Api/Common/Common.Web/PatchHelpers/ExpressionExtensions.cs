﻿using System;
using System.Linq.Expressions;

namespace Common.Web.PatchHelpers
{
    public static class ExpressionExtensions
    {
        public static string GetName<TSource, TField>(this Expression<Func<TSource, TField>> field)
        {
            return (field.Body as MemberExpression ?? ((UnaryExpression)field.Body).Operand as MemberExpression).Member.Name;
        }
    }
}
