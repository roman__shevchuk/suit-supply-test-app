﻿using Common.Web.PatchHelpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.ModelBinding.Binders;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Common.Web.Binders
{
    public class PatchJsonWithFilesFormDataModelBinder : IModelBinder
    {
        private readonly FormFileModelBinder formFileModelBinder;

        public PatchJsonWithFilesFormDataModelBinder(ILoggerFactory loggerFactory)
        {
            formFileModelBinder = new FormFileModelBinder(loggerFactory);
        }

        public async Task BindModelAsync(ModelBindingContext bindingContext)
        {
            if (bindingContext == null)
                throw new ArgumentNullException(nameof(bindingContext));

            if (!typeof(Changes).IsAssignableFrom(bindingContext.ModelType))
            {
                return;
            }

            // Retrieve the form part containing the JSON
            var valueResult = bindingContext.ValueProvider.GetValue(bindingContext.FieldName);
            if (valueResult == ValueProviderResult.None)
            {
                // The JSON was not found
                var message = bindingContext.ModelMetadata.ModelBindingMessageProvider.MissingBindRequiredValueAccessor(bindingContext.FieldName);
                bindingContext.ModelState.TryAddModelError(bindingContext.ModelName, message);
                return;
            }

            var model = (Changes)Activator.CreateInstance(bindingContext.ModelType);
            var modelPropertiesByName = bindingContext.ModelType.GenericTypeArguments[0]
                .GetProperties().ToDictionary(x => x.Name.ToUpperInvariant());

            RegisterJsonChanges(model, modelPropertiesByName, valueResult.FirstValue);
            await RegisterFormChanges(model, modelPropertiesByName, bindingContext);

            // Set the successfully constructed model as the result of the model binding
            bindingContext.Result = ModelBindingResult.Success(model);
        }

        private static void RegisterJsonChanges(Changes model, IDictionary<string, PropertyInfo> modelPropertiesByName, string modelJson)
        {
            var parsedModel = JObject.Parse(modelJson);
            foreach (JProperty jProperty in parsedModel.Properties())
            {
                if (modelPropertiesByName.TryGetValue(jProperty.Name.ToUpperInvariant(), out var propertyInfo))
                {
                    model.AddChange(propertyInfo, jProperty.Value.ToObject(propertyInfo.PropertyType));
                }
            }
        }

        private async Task RegisterFormChanges(Changes model, IDictionary<string, PropertyInfo> modelPropertiesByName, ModelBindingContext bindingContext)
        {
            var modelType = bindingContext.ModelType.GenericTypeArguments[0];
            var form = bindingContext.HttpContext.Request.Form;

            // searching for file properties in Changes.Entity
            var fileProperties = bindingContext.ModelMetadata
                .Properties.First(p => p.ModelType == modelType)
                .Properties.Where(p =>
                {
                    var propertyName = p.Name.ToUpperInvariant();
                    return p.ModelType == typeof(IFormFile) &&
                           (form.Keys.Any(k => k.ToUpperInvariant() == propertyName)
                            || form.Files.Any(f => f.Name.ToUpperInvariant() == propertyName));
                });

            // Now, bind each of the IFormFile properties from the other form parts
            foreach (var property in fileProperties)
            {
                var propertyName = property.BinderModelName ?? property.PropertyName;

                var propertyModel = property.PropertyGetter(bindingContext.Model);
                ModelBindingResult propertyResult;

                using (bindingContext.EnterNestedScope(property, propertyName, propertyName, propertyModel))
                {
                    await formFileModelBinder.BindModelAsync(bindingContext);
                    propertyResult = bindingContext.Result;
                }

                if (!modelPropertiesByName.TryGetValue(propertyName.ToUpperInvariant(), out var propertyInfo))
                    continue;

                if (propertyResult.IsModelSet)
                {
                    model.AddChange(propertyInfo, propertyResult.Model);
                }
                else if (property.IsBindingRequired)
                {
                    var message = property.ModelBindingMessageProvider.MissingBindRequiredValueAccessor(propertyName);
                    bindingContext.ModelState.TryAddModelError(propertyName, message);
                }
                else
                {
                    model.AddChange(propertyInfo, null);
                }
            }
        }
    }
}