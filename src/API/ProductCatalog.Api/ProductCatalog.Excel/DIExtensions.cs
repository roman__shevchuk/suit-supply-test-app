﻿using Microsoft.Extensions.DependencyInjection;
using System;

namespace ProductCatalog.Excel
{
    public static class DIExtensions
    {
        public static void AddExportServices(this IServiceCollection services)
        {
            services.AddScoped<IExportingService, ExportingService>();
            services.AddScoped<Lazy<IExportingService>>(sp =>
            {
                var service = sp.GetRequiredService<IExportingService>();
                return new Lazy<IExportingService>(service);
            });
        }
    }
}
