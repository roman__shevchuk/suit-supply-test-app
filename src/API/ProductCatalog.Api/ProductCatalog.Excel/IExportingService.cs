﻿using ProductCatalog.Domain.Dtos;
using System.Collections.Generic;
using System.IO;

namespace ProductCatalog.Excel
{
    public interface IExportingService
    {
        Stream Export(List<ProductDto> products);
    }
}
