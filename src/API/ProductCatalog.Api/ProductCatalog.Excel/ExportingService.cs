﻿using OfficeOpenXml;
using ProductCatalog.Domain.Dtos;
using System.Collections.Generic;
using System.Globalization;
using System.IO;

namespace ProductCatalog.Excel
{
    internal class ExportingService : IExportingService
    {
        private const string ColumnsFormat = "A{0}:G{1}";

        private const int IdColumnIndex = 1;
        private const int LastUpdatedColumnIndex = 2;
        private const int CodeColumnIndex = 3;
        private const int NameColumnIndex = 4;
        private const int PriceColumnIndex = 5;
        private const int PriceConfirmedColumnIndex = 6;


        public Stream Export(List<ProductDto> products)
        {
            var memoryStream = new MemoryStream();
            using (var package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add("Products");

                SetupHeader(worksheet);
                FillData(worksheet, products);

                worksheet.Cells[string.Format(ColumnsFormat, 1, products.Count + 2)].AutoFitColumns();
                package.SaveAs(memoryStream);
            }

            memoryStream.Seek(0, SeekOrigin.Begin);
            return memoryStream;
        }

        private static void SetupHeader(ExcelWorksheet worksheet)
        {
            worksheet.Cells[1, IdColumnIndex].Value = "ID";
            worksheet.Cells[1, LastUpdatedColumnIndex].Value = "Last updated";
            worksheet.Cells[1, CodeColumnIndex].Value = "Code";
            worksheet.Cells[1, NameColumnIndex].Value = "Name";
            worksheet.Cells[1, PriceColumnIndex].Value = "Price";
            worksheet.Cells[1, PriceConfirmedColumnIndex].Value = "Price confirmed";

            var headerColumns = string.Format(ColumnsFormat, 1, 1);
            worksheet.Cells[headerColumns].Style.Font.Size = 12;
            worksheet.Cells[headerColumns].Style.Font.Bold = true;
        }

        private static void FillData(ExcelWorksheet worksheet, IReadOnlyList<ProductDto> products)
        {
            for (var i = 0; i < products.Count; i++)
            {
                var index = i + 2;
                var product = products[i];

                worksheet.Cells[index, IdColumnIndex].Value = product.Id.ToString();
                worksheet.Cells[index, LastUpdatedColumnIndex].Value = product.LastUpdatedUtc.ToString(CultureInfo.InvariantCulture);
                worksheet.Cells[index, CodeColumnIndex].Value = product.Code;
                worksheet.Cells[index, NameColumnIndex].Value = product.Name;
                worksheet.Cells[index, PriceColumnIndex].Value = product.Price.ToString("C");
                worksheet.Cells[index, PriceConfirmedColumnIndex].Value = product.PriceConfirmed.ToString();
            }
        }
    }
}
