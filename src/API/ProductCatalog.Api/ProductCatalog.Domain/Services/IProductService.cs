﻿using Common.Web.PatchHelpers;
using ProductCatalog.Domain.Collections;
using ProductCatalog.Domain.Dtos;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ProductCatalog.Domain.Services
{
    public interface IProductService
    {
        Task<IEnumerable<ProductDto>> SearchProducts(
            string searchQuery,
            CancellationToken cancellationToken);

        Task<PagedList<ProductDto>> SearchProducts(
            ProductResourceParametersDto resourceParameters,
            CancellationToken cancellationToken);

        Task<ProductDto> GetProduct(
            long productId,
            CancellationToken cancellationToken);

        Task<ProductDto> CreateProduct(
            ProductForCreateDto creationDto,
            CancellationToken cancellationToken);

        Task<ProductDto> UpdateProduct(
            long productId,
            Changes<ProductForUpdateDto> patch,
            CancellationToken cancellationToken);

        Task<ProductDto> ConfirmPrice(
            long productId,
            CancellationToken cancellationToken);

        Task<bool> DeleteProduct(
            long productId,
            CancellationToken cancellationToken);
    }
}
