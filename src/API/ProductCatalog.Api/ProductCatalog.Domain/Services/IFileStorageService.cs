﻿using ProductCatalog.Domain.Models;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace ProductCatalog.Domain.Services
{
    public interface IFileStorageService
    {
        /// <summary>
        ///     Uploads a file to storage with specified name
        /// </summary>
        /// <returns>
        ///     File path in the storage
        /// </returns>
        Task<string> UploadFile(Stream stream, string filename, string folder, CancellationToken cancellationToken);

        /// <summary>
        ///     Creates URL with shared access signature
        /// </summary>
        Task<string> CreateSasUrl(string filePath, string originalFileName, CancellationToken cancellationToken);

        Task<string> CreateSasUrl(FileModel file, CancellationToken cancellationToken);

        Task<Stream> GetFileStream(string name, string folder, CancellationToken cancellationToken);

        Task<Stream> GetFileStream(string filePath, CancellationToken cancellationToken);

        Task DeleteFile(string filePath, CancellationToken cancellationToken);

        Task<bool> Exists(string filePath, CancellationToken cancellationToken);
    }
}
