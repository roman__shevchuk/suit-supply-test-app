﻿using Microsoft.Azure.Storage;
using Microsoft.Azure.Storage.Blob;
using MimeMapping;
using ProductCatalog.Domain.Models;
using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace ProductCatalog.Domain.Services
{
    internal class FileStorageService : IFileStorageService
    {
        private const string ContainerName = @"product-catalog";
        private readonly Lazy<CloudBlobContainer> blobContainer;
        private readonly SemaphoreSlim semaphoreSlim = new SemaphoreSlim(1, 1);

        public FileStorageService(Lazy<CloudStorageAccount> storageAccount)
        {
            var blobClient = new Lazy<CloudBlobClient>(() => storageAccount.Value.CreateCloudBlobClient());
            this.blobContainer = new Lazy<CloudBlobContainer>(() => blobClient.Value.GetContainerReference(ContainerName));
        }

        public async Task<string> UploadFile(
            Stream stream,
            string filename,
            string folder,
            CancellationToken cancellationToken)
        {
            var uniqueFileName =
                $"{Path.GetFileNameWithoutExtension(filename)}.{Guid.NewGuid()}{Path.GetExtension(filename)}";

            CloudBlobContainer container = await this.EnsureContainer().ConfigureAwait(false);

            var path = GetBlobPath(folder, uniqueFileName);
            CloudBlockBlob blockBlob = container.GetBlockBlobReference(path);

            await blockBlob.UploadFromStreamAsync(stream, cancellationToken).ConfigureAwait(false);

            return path;
        }

        public async Task<string> CreateSasUrl(string filepath, string originalFileName, CancellationToken cancellationToken)
        {
            var mimeType = MimeUtility.GetMimeMapping(originalFileName);
            var container = await this.EnsureContainer().ConfigureAwait(false);
            CloudBlockBlob blockBlob = container.GetBlockBlobReference(filepath);

            var sharedAccessBlobHeaders = new SharedAccessBlobHeaders
            {
                ContentDisposition = $"filename={HttpUtility.UrlEncode(originalFileName)}",
                ContentType = mimeType
            };

            var sharedAccessSignature = blockBlob.GetSharedAccessSignature(
                new SharedAccessBlobPolicy
                {
                    SharedAccessExpiryTime = DateTimeOffset.UtcNow + TimeSpan.FromDays(7),
                    Permissions = SharedAccessBlobPermissions.Read
                }, sharedAccessBlobHeaders);

            var sasUrl = blockBlob.Uri + sharedAccessSignature;
            return sasUrl;
        }

        public Task<string> CreateSasUrl(FileModel file, CancellationToken cancellationToken)
        {
            return this.CreateSasUrl(file.FilePath, file.OriginalFileName, cancellationToken);
        }

        public Task<Stream> GetFileStream(string name, string folder, CancellationToken cancellationToken)
        {
            var filePath = GetBlobPath(folder, name);
            return this.GetFileStream(filePath, cancellationToken);
        }

        public async Task<Stream> GetFileStream(string filePath, CancellationToken cancellationToken)
        {
            CloudBlobContainer container = await this.EnsureContainer().ConfigureAwait(false);
            CloudBlockBlob blockBlob = container.GetBlockBlobReference(filePath);

            return await blockBlob.OpenReadAsync(cancellationToken).ConfigureAwait(false);
        }

        public async Task DeleteFile(string filePath, CancellationToken cancellationToken)
        {
            CloudBlobContainer container = await this.EnsureContainer().ConfigureAwait(false);
            CloudBlockBlob blockBlob = container.GetBlockBlobReference(filePath);

            await blockBlob.DeleteAsync(cancellationToken).ConfigureAwait(false);
        }

        public async Task<bool> Exists(string filePath, CancellationToken cancellationToken)
        {
            var fileName = Path.GetFileName(filePath);
            if (string.IsNullOrEmpty(fileName))
            {
                throw new ArgumentException(@"File path does not contain file name.", nameof(filePath));
            }

            var container = await this.EnsureContainer().ConfigureAwait(false);
            CloudBlockBlob blockBlob = container.GetBlockBlobReference(filePath);
            return blockBlob.Exists();
        }

        private async Task<CloudBlobContainer> EnsureContainer()
        {
            if (this.blobContainer.IsValueCreated)
            {
                return this.blobContainer.Value;
            }

            this.semaphoreSlim.Wait();
            try
            {
                if (this.blobContainer.IsValueCreated)
                {
                    return this.blobContainer.Value;
                }

                await this.blobContainer.Value.CreateIfNotExistsAsync().ConfigureAwait(false);
                return this.blobContainer.Value;
            }
            finally
            {
                this.semaphoreSlim.Release();
            }
        }

        private static string GetBlobPath(string folder, string filename) => $"{folder}/{filename}";
    }
}
