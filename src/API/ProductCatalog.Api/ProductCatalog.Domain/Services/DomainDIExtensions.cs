﻿using Microsoft.Azure.Storage;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace ProductCatalog.Domain.Services
{
    public static class DomainDIExtensions
    {
        public static void AddDomainLayer(this IServiceCollection services, string storageAccountConnectionString)
        {
            services.AddSingleton<IFileStorageService, FileStorageService>(sp =>
            {
                var storageAccount = CloudStorageAccount.Parse(storageAccountConnectionString);
                return new FileStorageService(new Lazy<CloudStorageAccount>(storageAccount));
            });
            services.AddSingleton<Lazy<IFileStorageService>>(sp =>
            {
                var service = sp.GetRequiredService<IFileStorageService>();
                return new Lazy<IFileStorageService>(service);
            });

            services.AddScoped<IProductService, ProductService>();
            services.AddScoped<Lazy<IProductService>>(sp =>
            {
                var service = sp.GetRequiredService<IProductService>();
                return new Lazy<IProductService>(service);
            });
        }
    }
}
