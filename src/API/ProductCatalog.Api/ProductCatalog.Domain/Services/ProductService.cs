﻿using AutoMapper;
using Common.Web.PatchHelpers;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using ProductCatalog.Domain.Collections;
using ProductCatalog.Domain.Dtos;
using ProductCatalog.Domain.Mappings;
using ProductCatalog.Domain.Models;
using ProductCatalog.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ProductCatalog.Domain.Services
{
    public class ProductService : IProductService
    {
        private const string ImagesFolder = "images";

        private readonly IProductRepository productRepository;
        private readonly IFileRepository fileRepository;
        private readonly IFileStorageService fileStorageService;

        private static readonly Lazy<IMapper> Mapper = new Lazy<IMapper>(() => new Mapper(new MapperConfiguration(cfg => cfg.AddProfile<ProductMappingProfile>())));

        public ProductService(
            IProductRepository productRepository,
            IFileRepository fileRepository,
            IFileStorageService fileStorageService)
        {
            this.productRepository = productRepository;
            this.fileRepository = fileRepository;
            this.fileStorageService = fileStorageService;
        }
        public async Task<IEnumerable<ProductDto>> SearchProducts(
            string searchQuery,
            CancellationToken cancellationToken)
        {
            var query = this.productRepository.Query()
                .Include(x => x.Image)
                .AsQueryable();

            query = ApplySearch(query, searchQuery);

            var products = await query.ToListAsync(cancellationToken);

            var tasks = products.Select(p => MapToDto(p, cancellationToken)).ToList();
            await Task.WhenAll(tasks);

            return tasks.Select(t => t.Result);
        }


        public async Task<PagedList<ProductDto>> SearchProducts(
            ProductResourceParametersDto resourceParameters,
            CancellationToken cancellationToken)
        {
            var query = this.productRepository.Query()
                .Include(x => x.Image)
                .AsQueryable();

            query = ApplySearch(query, resourceParameters.SearchQuery);

            var count = await query.CountAsync(cancellationToken);
            var products = await query.Skip((resourceParameters.PageNumber - 1) * resourceParameters.PageSize)
                .Take(resourceParameters.PageSize)
                .ToListAsync(cancellationToken);

            var tasks = products.Select(p => MapToDto(p, cancellationToken)).ToList();
            await Task.WhenAll(tasks);

            return new PagedList<ProductDto>(
                tasks.Select(t => t.Result),
                count,
                resourceParameters.PageNumber,
                resourceParameters.PageSize);
        }

        public async Task<ProductDto> GetProduct(long productId, CancellationToken cancellationToken)
        {
            var product = await this.productRepository.Query()
                .Include(x => x.Image)
                .FirstOrDefaultAsync(p => p.Id == productId, cancellationToken);

            if (product == null)
            {
                return null;
            }

            return await this.MapToDto(product, cancellationToken);
        }

        public async Task<ProductDto> CreateProduct(ProductForCreateDto creationDto, CancellationToken cancellationToken)
        {
            await CheckCodeDuplicates(creationDto.Code, cancellationToken);

            var product = Mapper.Value.Map<ProductModel>(creationDto);
            product.UpdatePriceConfirmationRequirement();
            product.Image = await UploadFile(creationDto.Image, ImagesFolder, cancellationToken);

            this.productRepository.Add(product);
            await this.productRepository.SaveChanges(cancellationToken);

            return await this.MapToDto(product, cancellationToken);
        }

        public async Task<ProductDto> UpdateProduct(
            long productId,
            Changes<ProductForUpdateDto> patch,
            CancellationToken cancellationToken)
        {
            if (patch.HasChanged(x => x.Code))
            {
                await CheckCodeDuplicates(patch.Entity.Code, cancellationToken);
            }

            var product = await this.productRepository.Query()
                .Include(x => x.Image)
                .FirstOrDefaultAsync(p => p.Id == productId, cancellationToken);

            if (product == null)
            {
                return null;
            }

            var update = Mapper.Value.Map<ProductForUpdateDto>(product);
            patch.ApplyTo(update);
            Mapper.Value.Map(update, product);

            if (patch.HasChanged(x => x.Price))
            {
                product.UpdatePriceConfirmationRequirement();
            }

            if (patch.HasChanged(x => x.Image))
            {
                var newImage = await UploadFile(patch.Entity.Image, ImagesFolder, cancellationToken);
                await UpdateFile(product, newImage, cancellationToken);
            }

            product.SetLastUpdateTime();
            await this.productRepository.SaveChanges(cancellationToken);
            return await MapToDto(product, cancellationToken);
        }

        public async Task<ProductDto> ConfirmPrice(long productId, CancellationToken cancellationToken)
        {
            var product = await this.productRepository.Query()
                .Include(x => x.Image)
                .FirstOrDefaultAsync(p => p.Id == productId, cancellationToken);

            if (product == null)
            {
                return null;
            }

            product.PriceConfirmed = true;
            product.SetLastUpdateTime();
            await this.productRepository.SaveChanges(cancellationToken);

            return await MapToDto(product, cancellationToken);
        }

        public async Task<bool> DeleteProduct(long productId, CancellationToken cancellationToken)
        {
            var product = await this.productRepository.Query()
                .Include(x => x.Image)
                .FirstOrDefaultAsync(p => p.Id == productId, cancellationToken);

            if (product == null)
            {
                return false;
            }

            this.productRepository.Remove(product);
            if (product.Image != null)
            {
                await this.RemoveFile(product.Image, cancellationToken);
            }

            await this.productRepository.SaveChanges(cancellationToken);

            return true;
        }


        private static IQueryable<ProductModel> ApplySearch(IQueryable<ProductModel> query, string searchQuery)
        {
            return !string.IsNullOrWhiteSpace(searchQuery)
                ? query.Where(p => p.Code.Contains(searchQuery) || p.Name.Contains(searchQuery))
                : query;
        }

        private async Task CheckCodeDuplicates(string code, CancellationToken cancellationToken)
        {
            var isCodeDuplicated = await this.productRepository.Query()
                .AnyAsync(x => x.Code == code, cancellationToken);

            if (isCodeDuplicated)
            {
                throw new ArgumentException("Specified code already exists.", nameof(ProductForCreateDto.Code));
            }
        }

        private async Task<FileModel> UploadFile(
            IFormFile file,
            string folder,
            CancellationToken cancellationToken)
        {
            if (file?.Length > 0)
            {
                using (Stream fileStream = file.OpenReadStream())
                {
                    var filePath = await this.fileStorageService.UploadFile(
                        fileStream,
                        file.FileName,
                        folder,
                        cancellationToken);

                    return new FileModel(file.FileName, filePath);
                }
            }
            else
            {
                return null;
            }
        }

        private async Task RemoveFile(FileModel image, CancellationToken cancellationToken)
        {
            if (image == null)
                return;

            await this.fileStorageService.DeleteFile(image.FilePath, cancellationToken);
            this.fileRepository.Remove(image);
        }

        private async Task UpdateFile(ProductModel product, FileModel newImage, CancellationToken cancellationToken)
        {
            if (newImage == null)
            {
                await RemoveFile(product.Image, cancellationToken);
                product.Image = null;
            }
            else if (product.Image != null)
            {
                await this.fileStorageService.DeleteFile(product.Image.FilePath, cancellationToken);

                product.Image.FilePath = newImage.FilePath;
                product.Image.OriginalFileName = newImage.OriginalFileName;
            }
            else
            {
                product.Image = newImage;
            }
        }

        private async Task<ProductDto> MapToDto(
            ProductModel product,
            CancellationToken cancellationToken)
        {
            var dto = Mapper.Value.Map<ProductDto>(product);
            if (product.Image == null)
                return dto;

            dto.ImageUrl = await this.fileStorageService.CreateSasUrl(product.Image.FilePath, product.Image.OriginalFileName, cancellationToken);
            return dto;
        }
    }
}
