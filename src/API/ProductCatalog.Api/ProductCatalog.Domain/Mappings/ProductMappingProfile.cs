﻿using AutoMapper;
using ProductCatalog.Domain.Dtos;
using ProductCatalog.Domain.Models;
using System;

namespace ProductCatalog.Domain.Mappings
{
    public class ProductMappingProfile : Profile
    {
        public ProductMappingProfile()
        {
            CreateMap<ProductModel, ProductDto>()
                .ForMember(d => d.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(d => d.Code, opt => opt.MapFrom(src => src.Code))
                .ForMember(d => d.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(d => d.Price, opt => opt.MapFrom(src => src.Price))
                .ForMember(d => d.PriceConfirmed, opt => opt.MapFrom(src => src.PriceConfirmed))
                .ForMember(d => d.CreatedUtc, opt => opt.MapFrom(src => DateTime.SpecifyKind(src.CreatedUtc, DateTimeKind.Utc)))
                .ForMember(d => d.LastUpdatedUtc, opt => opt.MapFrom(src => DateTime.SpecifyKind(src.LastUpdatedUtc, DateTimeKind.Utc)));

            CreateMap<ProductForCreateDto, ProductModel>()
                .ForMember(d => d.Code, opt => opt.MapFrom(src => src.Code))
                .ForMember(d => d.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(d => d.Price, opt => opt.MapFrom(src => src.Price))
                .ForMember(d => d.Image, opt => opt.Ignore());

            CreateMap<ProductForUpdateDto, ProductModel>()
                .ForMember(d => d.Code, opt => opt.MapFrom(src => src.Code))
                .ForMember(d => d.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(d => d.Price, opt => opt.MapFrom(src => src.Price))
                .ForMember(d => d.Image, opt => opt.Ignore())
                .ReverseMap()
                .ForMember(d => d.Code, opt => opt.MapFrom(src => src.Code))
                .ForMember(d => d.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(d => d.Price, opt => opt.MapFrom(src => src.Price))
                .ForMember(d => d.Image, opt => opt.Ignore());
        }
    }
}
