﻿using ProductCatalog.Domain.Models;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ProductCatalog.Domain.Repositories
{
    public interface IFileRepository
    {
        IQueryable<FileModel> Query();

        void Remove(FileModel file);

        Task SaveChanges(CancellationToken cancellationToken);
    }
}
