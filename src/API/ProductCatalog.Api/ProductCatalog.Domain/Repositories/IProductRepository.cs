﻿using ProductCatalog.Domain.Models;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ProductCatalog.Domain.Repositories
{
    public interface IProductRepository
    {
        IQueryable<ProductModel> Query();

        void Add(ProductModel product);

        void Remove(ProductModel product);

        Task SaveChanges(CancellationToken cancellationToken);
    }
}
