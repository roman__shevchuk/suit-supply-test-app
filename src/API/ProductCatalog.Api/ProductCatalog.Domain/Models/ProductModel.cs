﻿using System;

namespace ProductCatalog.Domain.Models
{
    public class ProductModel : Model
    {
        private const decimal MaxPriceAllowedWithoutConfirm = 999;

        public string Code { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public bool PriceConfirmed { get; set; }
        public virtual FileModel Image { get; set; }

        public void UpdatePriceConfirmationRequirement()
        {
            this.PriceConfirmed = this.Price <= MaxPriceAllowedWithoutConfirm;
        }

        public void SetLastUpdateTime()
        {
            this.LastUpdatedUtc = DateTime.UtcNow;
        }
    }
}
