﻿using System;
using System.IO;
using System.Linq;

namespace ProductCatalog.Domain.Models
{
    public class FileModel : Model
    {
        public FileModel(string originalFileName, string filePath)
        {
            this.OriginalFileName = originalFileName ?? throw new ArgumentNullException(nameof(originalFileName));
            this.FilePath = filePath ?? throw new ArgumentNullException(nameof(filePath));
        }

        private FileModel()
        {
        }

        public string OriginalFileName { get; set; }

        public string FilePath { get; set; }

        public bool IsEmpty => string.IsNullOrEmpty(this.FilePath);

        public static FileModel Empty => new FileModel();

        public bool IsFileOfType(params string[] fileExtensions)
        {
            var fileExtension = Path.GetExtension(this.FilePath)?.ToUpperInvariant();
            return fileExtensions.Any(ext => fileExtension == ext);
        }

        public FileModel Clone()
        {
            return new FileModel(this.OriginalFileName, this.FilePath);
        }
    }
}
