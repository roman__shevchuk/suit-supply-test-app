﻿using System;

namespace ProductCatalog.Domain.Models
{
    public class Model
    {
        public long Id { get; set; }

        public DateTime CreatedUtc { get; protected set; }

        public DateTime LastUpdatedUtc { get; protected set; }
    }
}
