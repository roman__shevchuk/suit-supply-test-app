﻿namespace ProductCatalog.Domain.Dtos
{
    public class ProductResourceParametersDto
    {
        private const int MaxPageSize = 20;
        private const int MinValue = 1;

        private int pageSize = 10;
        private int pageNumber = 1;

        public int PageNumber
        {
            get => pageNumber;
            set => pageNumber = value < MinValue ? MinValue : value;
        }
        public int PageSize
        {
            get => pageSize;
            set
            {
                if (value >= MinValue && value <= MaxPageSize)
                {
                    pageSize = value;
                }
                else
                {
                    pageSize = value < MinValue ? MinValue : MaxPageSize;
                }
            }
        }

        public string SearchQuery { get; set; }
    }
}
