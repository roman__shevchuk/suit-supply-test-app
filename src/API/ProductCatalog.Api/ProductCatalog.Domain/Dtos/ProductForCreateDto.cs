﻿using Common.Web.Binders;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ProductCatalog.Domain.Dtos
{
    [ModelBinder(typeof(JsonWithFilesFormDataModelBinder), Name = ModelBinderConstants.JsonPropertyName)]
    public class ProductForCreateDto
    {
        public string Name { get; set; }

        public string Code { get; set; }

        public decimal Price { get; set; }

        public IFormFile Image { get; set; }
    }
}
