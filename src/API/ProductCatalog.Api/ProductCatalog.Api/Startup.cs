using AutoMapper;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using ProductCatalog.Api.Configuration;
using ProductCatalog.Domain.Services;
using ProductCatalog.Excel;
using ProductCatalog.Persistence.DI;

namespace ProductCatalog.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var loggerFactory = LoggerFactory.Create(builder => builder.AddConsole())
                .AddFile(
                    Configuration.GetValue<string>("Logging:File"),
                    LogLevel.Debug);

            services.AddSingleton(loggerFactory);
            services.AddLogging();

            services
                .AddControllers(options =>
                {
                    //options.ReturnHttpNotAcceptable = true;

                    options.Filters.Add(
                        new ProducesResponseTypeAttribute(StatusCodes.Status400BadRequest));
                    options.Filters.Add(
                        new ProducesResponseTypeAttribute(StatusCodes.Status406NotAcceptable));
                    options.Filters.Add(
                        new ProducesResponseTypeAttribute(StatusCodes.Status500InternalServerError));
                    options.Filters.Add(
                        new ProducesDefaultResponseTypeAttribute());
                })
                .AddNewtonsoftJson()
                .AddXmlDataContractSerializerFormatters()
                .AddFluentValidation(options => options.RegisterValidatorsFromAssemblyContaining<Startup>());

            services.AddAutoMapper(typeof(Startup));
            services.AddCors();

            services.AddPersistenceLayer(loggerFactory, Configuration.GetConnectionString("ProductCatalogDb"));
            services.AddDomainLayer(Configuration.GetConnectionString("FileStorage"));

            services.AddExportServices();

            services.AddProductCatalogVersioning();
            services.AddProductCatalogSwagger();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(
            IApplicationBuilder app,
            IWebHostEnvironment env,
            IApiVersionDescriptionProvider versionDescriptionProvider,
            ILogger<Startup> logger)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UpdateProductCatalogDatabase();

            app.ConfigureExceptionHandler(logger);

            app.UseCors(b => b.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader().WithExposedHeaders("X-Pagination"));
            app.UseRouting();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseSwaggerForProductCatalog(versionDescriptionProvider);
        }
    }
}
