﻿using Common.Web.PatchHelpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MimeMapping;
using ProductCatalog.Domain.Dtos;
using ProductCatalog.Domain.Services;
using ProductCatalog.Excel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ProductCatalog.Api.Controllers
{
    [ApiController]
    [Produces("application/json", "application/xml")]
    [Route("api/v{version:apiVersion}/products")]
    public class ProductsController : ControllerBase
    {
        private readonly Lazy<IProductService> productService;
        private readonly Lazy<IExportingService> exportingService;

        public ProductsController(
            Lazy<IProductService> productService,
            Lazy<IExportingService> exportingService)
        {
            this.productService = productService;
            this.exportingService = exportingService;
        }

        [ProducesResponseType(StatusCodes.Status200OK)]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ProductDto>>> SearchProducts(
            [FromQuery]ProductResourceParametersDto resourceParameters,
            CancellationToken cancellationToken)
        {
            var products = await productService.Value.SearchProducts(resourceParameters, cancellationToken);
            var paginationMetadata = new
            {
                totalCount = products.TotalCount,
                pageSize = products.PageSize,
                currentPage = products.CurrentPage,
                totalPages = products.TotalPages
            };

            Response.Headers.Add("X-Pagination",
                Newtonsoft.Json.JsonConvert.SerializeObject(paginationMetadata));

            return Ok(products);
        }

        [ProducesResponseType(StatusCodes.Status200OK)]
        [HttpGet("excel")]
        public async Task<FileStreamResult> ExportToExcel(
            string searchQuery,
            CancellationToken cancellationToken)
        {
            var products = await productService.Value.SearchProducts(searchQuery, cancellationToken);
            var stream = exportingService.Value.Export(products.ToList());

            return File(stream, KnownMimeTypes.Xlsx);
        }

        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{productId}", Name = nameof(GetProductById))]
        public async Task<ActionResult<ProductDto>> GetProductById(long productId, CancellationToken cancellationToken)
        {
            var product = await this.productService.Value.GetProduct(productId, cancellationToken);

            if (product == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(product);
            }
        }

        [ProducesResponseType(StatusCodes.Status201Created)]
        [HttpPost]
        public async Task<ActionResult<ProductDto>> CreateProduct(ProductForCreateDto product, ApiVersion apiVersion, CancellationToken cancellationToken)
        {
            if (product == null)
            {
                return BadRequest();
            }

            var newProduct = await productService.Value.CreateProduct(product, cancellationToken);

            return CreatedAtRoute(
                nameof(GetProductById),
                new { productId = newProduct.Id, version = apiVersion.ToString() },
                newProduct);
        }

        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPatch("{productId}")]
        public async Task<ActionResult<ProductDto>> UpdateProduct(
            long productId,
            Changes<ProductForUpdateDto> patch,
            CancellationToken cancellationToken)
        {
            if (patch == null)
            {
                return BadRequest();
            }

            var updatedProduct = await productService.Value.UpdateProduct(productId, patch, cancellationToken);

            if (updatedProduct == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(updatedProduct);
            }
        }

        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPatch("{productId}/priceConfirmation")]
        public async Task<ActionResult<ProductDto>> ConfirmPrice(long productId, CancellationToken cancellationToken)
        {
            var updatedProduct = await productService.Value.ConfirmPrice(productId, cancellationToken);

            if (updatedProduct == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(updatedProduct);
            }
        }

        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{productId}")]
        public async Task<ActionResult> DeleteProduct(long productId, CancellationToken cancellationToken)
        {
            if (!await this.productService.Value.DeleteProduct(productId, cancellationToken))
            {
                return NotFound();
            }
            else
            {
                return NoContent();
            }
        }
    }
}
