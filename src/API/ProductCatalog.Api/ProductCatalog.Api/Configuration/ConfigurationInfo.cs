﻿using System.Reflection;

namespace ProductCatalog.Api.Configuration
{
    public static class ConfigurationInfo
    {
        public static string Name { get; } = Assembly.GetExecutingAssembly()
            .GetName()
            .Name;

        public const string Title = "Products catalog";
        public const string Description = "API that allows to interact with catalog of products";
    }
}
