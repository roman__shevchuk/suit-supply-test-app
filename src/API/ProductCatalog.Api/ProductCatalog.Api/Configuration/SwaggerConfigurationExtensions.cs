﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc.Abstractions;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using System;
using System.IO;
using System.Linq;
using System.Reflection;

namespace ProductCatalog.Api.Configuration
{
    public static class SwaggerConfigurationExtensions
    {
        public static void AddProductCatalogSwagger(this IServiceCollection services)
        {
            var versionDescriptionProvider =
                services.BuildServiceProvider()
                    .GetService<IApiVersionDescriptionProvider>();

            services.AddSwaggerGen(setupAction =>
            {
                setupAction.DocInclusionPredicate((documentName, apiDescription) =>
                {
                    var actionApiVersionModel = apiDescription.ActionDescriptor
                        .GetApiVersionModel(ApiVersionMapping.Explicit | ApiVersionMapping.Implicit);

                    if (actionApiVersionModel == null)
                    {
                        return true;
                    }

                    if (actionApiVersionModel.DeclaredApiVersions.Any())
                    {
                        return actionApiVersionModel.DeclaredApiVersions.Any(v =>
                            $"{ConfigurationInfo.Name}v{v.ToString()}" == documentName);
                    }

                    return actionApiVersionModel.ImplementedApiVersions.Any(v =>
                        $"{ConfigurationInfo.Name}v{v.ToString()}" == documentName);
                });

                foreach (var description in versionDescriptionProvider.ApiVersionDescriptions)
                {
                    setupAction.SwaggerDoc(
                        $"{ConfigurationInfo.Name}{description.GroupName}",
                        new OpenApiInfo
                        {
                            Title = ConfigurationInfo.Title,
                            Description = ConfigurationInfo.Description,
                            Version = description.ApiVersion.ToString()
                        });
                }

                var xmlCommentsFileName = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlCommentsFilePath = Path.Combine(AppContext.BaseDirectory, xmlCommentsFileName);

                setupAction.IncludeXmlComments(xmlCommentsFilePath);
            });
        }

        public static void UseSwaggerForProductCatalog(
            this IApplicationBuilder app,
            IApiVersionDescriptionProvider versionDescriptionProvider)
        {
            app.UseSwagger();
            app.UseSwaggerUI(setupAction =>
            {

                foreach (var description in versionDescriptionProvider.ApiVersionDescriptions)
                {
                    setupAction.SwaggerEndpoint(
                        $"/swagger/{ConfigurationInfo.Name}{description.GroupName}/swagger.json",
                        description.GroupName.ToUpperInvariant());
                }

                setupAction.RoutePrefix = string.Empty;
            });
        }

    }
}
