﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using System;
using System.Net;
using System.Threading.Tasks;

namespace ProductCatalog.Api.Configuration
{
    public static class ExceptionMiddlewareExtensions
    {
        public static void ConfigureExceptionHandler(this IApplicationBuilder app, ILogger logger)
        {
            app.UseExceptionHandler(appError =>
            {
                appError.Run(async context =>
                {
                    context.Response.ContentType = "application/json";

                    var contextFeature = context.Features.Get<IExceptionHandlerFeature>();
                    if (contextFeature.Error is ArgumentException argumentException)
                    {
                        await ProcessArgumentException(context, argumentException, logger);
                    }
                    else
                    {
                        await ProcessUnhandledException(context, contextFeature.Error, logger);
                    }
                });
            });
        }

        private static async Task ProcessUnhandledException(HttpContext context, Exception exception, ILogger logger)
        {
            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;

            logger.LogError($"Something went wrong: {exception}");
            await context.Response.WriteAsync("Something went wrong. Please, contact system administrator.");
        }

        private static async Task ProcessArgumentException(HttpContext context, ArgumentException argumentException, ILogger logger)
        {
            context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
            logger.LogError(argumentException.Message);

            var jObject = new JObject
            {
                ["errors"] = new JObject
                {
                    [argumentException.ParamName] = new JArray(argumentException.Message)
                }
            };
            await context.Response.WriteAsync(jObject.ToString());
        }
    }
}
