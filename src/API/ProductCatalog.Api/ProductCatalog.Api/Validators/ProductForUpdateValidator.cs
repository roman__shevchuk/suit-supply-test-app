﻿using Common.Web.PatchHelpers;
using FluentValidation;
using ProductCatalog.Domain.Dtos;

namespace ProductCatalog.Api.Validators
{
    public class ProductForUpdateValidator : AbstractValidator<Changes<ProductForUpdateDto>>
    {
        public ProductForUpdateValidator()
        {
            When(
                x => x.HasChanged(p => p.Code),
                () => RuleFor(x => x.Entity.Code).NotEmpty().MaximumLength(50).OverridePropertyName(nameof(ProductForUpdateDto.Code)));
            When(
                x => x.HasChanged(p => p.Name),
                () => RuleFor(x => x.Entity.Name).NotEmpty().OverridePropertyName(nameof(ProductForUpdateDto.Name)).MaximumLength(300));
            When(
                x => x.HasChanged(p => p.Price),
                () => RuleFor(x => x.Entity.Price).GreaterThan(0).OverridePropertyName(nameof(ProductForUpdateDto.Price)));
            When(
                x => x.HasChanged(p => p.Image),
                () => RuleFor(x => x.Entity.Image).SetValidator(new ImageFileValidator()).OverridePropertyName(nameof(ProductForUpdateDto.Image)));
        }
    }
}
