﻿using FluentValidation;
using ProductCatalog.Domain.Dtos;

namespace ProductCatalog.Api.Validators
{
    public class ProductForCreateValidator : AbstractValidator<ProductForCreateDto>
    {
        public ProductForCreateValidator()
        {
            RuleFor(x => x.Code).NotEmpty().MaximumLength(50).OverridePropertyName(nameof(ProductForCreateDto.Code));
            RuleFor(x => x.Name).NotEmpty().MaximumLength(300).OverridePropertyName(nameof(ProductForCreateDto.Name));
            RuleFor(x => x.Price).GreaterThan(0).OverridePropertyName(nameof(ProductForCreateDto.Price));
            RuleFor(x => x.Image).SetValidator(new ImageFileValidator()).OverridePropertyName(nameof(ProductForCreateDto.Image));
        }
    }
}
