﻿using FluentValidation;
using Microsoft.AspNetCore.Http;
using System.IO;
using System.Linq;

namespace ProductCatalog.Api.Validators
{
    public class ImageFileValidator : AbstractValidator<IFormFile>
    {
        private static readonly string[] AllowedExtensions = { ".jpg", ".jpeg", ".png" };

        public ImageFileValidator()
        {
            RuleFor(x => x).Custom((file, context) =>
            {
                if (file.Length <= 0)
                    return;

                var extension = Path.GetExtension(file.FileName).ToLower();
                if (AllowedExtensions.Contains(extension))
                    return;

                context.AddFailure(context.PropertyName, $"Invalid image extension: {extension}");
            });
        }
    }
}
