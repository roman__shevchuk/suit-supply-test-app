﻿using ProductCatalog.Domain.Models;
using ProductCatalog.Domain.Repositories;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ProductCatalog.Persistence.Repositories
{
    internal class FileRepository : IFileRepository
    {
        private readonly Lazy<ProductCatalogDbContext> dbContext;

        public FileRepository(Lazy<ProductCatalogDbContext> dbContext)
        {
            this.dbContext = dbContext;
        }

        public IQueryable<FileModel> Query()
        {
            return dbContext.Value.Files.AsQueryable();
        }

        public void Remove(FileModel file)
        {
            dbContext.Value.Files.Remove(file);
        }

        public Task SaveChanges(CancellationToken cancellationToken)
        {
            return dbContext.Value.SaveChangesAsync(cancellationToken);
        }
    }
}
