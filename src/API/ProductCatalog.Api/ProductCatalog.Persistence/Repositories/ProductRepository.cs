﻿using ProductCatalog.Domain.Models;
using ProductCatalog.Domain.Repositories;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ProductCatalog.Persistence.Repositories
{
    internal class ProductRepository : IProductRepository
    {
        private readonly Lazy<ProductCatalogDbContext> dbContext;

        public ProductRepository(Lazy<ProductCatalogDbContext> dbContext)
        {
            this.dbContext = dbContext;
        }

        public IQueryable<ProductModel> Query()
        {
            return dbContext.Value.Products.AsQueryable();
        }

        public void Add(ProductModel product)
        {
            dbContext.Value.Products.Add(product);
        }

        public void Remove(ProductModel product)
        {
            dbContext.Value.Products.Remove(product);
        }

        public Task SaveChanges(CancellationToken cancellationToken)
        {
            return dbContext.Value.SaveChangesAsync(cancellationToken);
        }
    }
}
