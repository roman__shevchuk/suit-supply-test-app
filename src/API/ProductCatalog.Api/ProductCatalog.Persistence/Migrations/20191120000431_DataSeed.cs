﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ProductCatalog.Persistence.Migrations
{
    public partial class DataSeed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 1L, "RS-1", null, "Product №$1", 1861.9208232788m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 659L, "RS-659", null, "Product №$659", 1676.3548886759m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 660L, "RS-660", null, "Product №$660", 9868.21352963718m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 661L, "RS-661", null, "Product №$661", 7924.08510014605m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 662L, "RS-662", null, "Product №$662", 1061.21473063771m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 663L, "RS-663", null, "Product №$663", 1488.8745879237m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 664L, "RS-664", null, "Product №$664", 7835.72433881262m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 665L, "RS-665", null, "Product №$665", 7149.97290500904m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 666L, "RS-666", null, "Product №$666", 4271.2219545018m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 667L, "RS-667", null, "Product №$667", 8641.42325177855m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 668L, "RS-668", null, "Product №$668", 651.019895752436m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 669L, "RS-669", null, "Product №$669", 6700.82843708844m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 670L, "RS-670", null, "Product №$670", 5989.30948227146m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 671L, "RS-671", null, "Product №$671", 2663.05494711877m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 672L, "RS-672", null, "Product №$672", 6138.87235808134m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 673L, "RS-673", null, "Product №$673", 1931.30103495498m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 674L, "RS-674", null, "Product №$674", 3025.57656216695m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 675L, "RS-675", null, "Product №$675", 2955.17104349806m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 676L, "RS-676", null, "Product №$676", 893.800817846228m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 677L, "RS-677", null, "Product №$677", 3993.34605037856m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 678L, "RS-678", null, "Product №$678", 4256.93352904959m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 679L, "RS-679", null, "Product №$679", 1410.09131977805m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 680L, "RS-680", null, "Product №$680", 8555.46861354051m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 681L, "RS-681", null, "Product №$681", 8773.61335734539m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 682L, "RS-682", null, "Product №$682", 5029.11238699644m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 683L, "RS-683", null, "Product №$683", 3151.69084963933m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 684L, "RS-684", null, "Product №$684", 7634.86419694259m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 685L, "RS-685", null, "Product №$685", 9111.4789848735m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 658L, "RS-658", null, "Product №$658", 4829.72706427319m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 686L, "RS-686", null, "Product №$686", 4983.11472357396m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 657L, "RS-657", null, "Product №$657", 6639.0485906224m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 655L, "RS-655", null, "Product №$655", 4546.47234387066m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 628L, "RS-628", null, "Product №$628", 5344.9739959766m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 629L, "RS-629", null, "Product №$629", 3185.57059075011m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 630L, "RS-630", null, "Product №$630", 7506.4054771822m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 631L, "RS-631", null, "Product №$631", 4431.12379146326m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 632L, "RS-632", null, "Product №$632", 9782.09288780675m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 633L, "RS-633", null, "Product №$633", 6861.48999112728m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 634L, "RS-634", null, "Product №$634", 5058.47788185742m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 635L, "RS-635", null, "Product №$635", 418.016007364735m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 636L, "RS-636", null, "Product №$636", 3880.64228644624m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 637L, "RS-637", null, "Product №$637", 9446.34497139898m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 638L, "RS-638", null, "Product №$638", 6068.56513119702m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 639L, "RS-639", null, "Product №$639", 7111.90345096956m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 640L, "RS-640", null, "Product №$640", 5127.34150752767m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 641L, "RS-641", null, "Product №$641", 1888.83764291594m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 642L, "RS-642", null, "Product №$642", 9355.61564720963m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 643L, "RS-643", null, "Product №$643", 5318.67283644093m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 644L, "RS-644", null, "Product №$644", 1433.85611075622m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 645L, "RS-645", null, "Product №$645", 4686.91603964517m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 646L, "RS-646", null, "Product №$646", 8104.62774620607m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 647L, "RS-647", null, "Product №$647", 425.102687638766m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 648L, "RS-648", null, "Product №$648", 2142.82133250629m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 649L, "RS-649", null, "Product №$649", 2193.28314633727m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 650L, "RS-650", null, "Product №$650", 5550.70639380752m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 651L, "RS-651", null, "Product №$651", 8394.9264923087m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 652L, "RS-652", null, "Product №$652", 9448.00906788931m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 653L, "RS-653", null, "Product №$653", 5533.62778645643m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 654L, "RS-654", null, "Product №$654", 3526.5653596849m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 656L, "RS-656", null, "Product №$656", 5329.44357270815m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 687L, "RS-687", null, "Product №$687", 4248.46510135032m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 688L, "RS-688", null, "Product №$688", 3334.92463144237m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 689L, "RS-689", null, "Product №$689", 512.005537986758m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 722L, "RS-722", null, "Product №$722", 5306.49862033618m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 723L, "RS-723", null, "Product №$723", 139.014357765678m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 724L, "RS-724", null, "Product №$724", 1612.25600243185m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 725L, "RS-725", null, "Product №$725", 8747.71578644762m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 726L, "RS-726", null, "Product №$726", 8046.43703999298m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 727L, "RS-727", null, "Product №$727", 1746.66211556022m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 728L, "RS-728", null, "Product №$728", 4687.6111136226m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 729L, "RS-729", null, "Product №$729", 5822.32015478533m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 730L, "RS-730", null, "Product №$730", 2127.54813121983m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 731L, "RS-731", null, "Product №$731", 3027.05975856029m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 732L, "RS-732", null, "Product №$732", 6510.39755275026m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 733L, "RS-733", null, "Product №$733", 9973.05032330242m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 734L, "RS-734", null, "Product №$734", 994.397234634681m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 735L, "RS-735", null, "Product №$735", 9092.26411911299m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 736L, "RS-736", null, "Product №$736", 8999.53056545906m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 737L, "RS-737", null, "Product №$737", 9587.1194915786m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 738L, "RS-738", null, "Product №$738", 6947.71718557352m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 739L, "RS-739", null, "Product №$739", 4747.21275025383m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 740L, "RS-740", null, "Product №$740", 6855.42485064614m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 741L, "RS-741", null, "Product №$741", 7466.40669063963m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 742L, "RS-742", null, "Product №$742", 1740.41387706083m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 743L, "RS-743", null, "Product №$743", 2763.53031525553m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 744L, "RS-744", null, "Product №$744", 6859.33401196233m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 745L, "RS-745", null, "Product №$745", 3752.474912327m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 746L, "RS-746", null, "Product №$746", 4859.47863425104m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 747L, "RS-747", null, "Product №$747", 1196.98216263064m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 748L, "RS-748", null, "Product №$748", 1271.32396738572m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 721L, "RS-721", null, "Product №$721", 22.7568531514876m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 720L, "RS-720", null, "Product №$720", 2166.85818143508m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 719L, "RS-719", null, "Product №$719", 8724.24535393913m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 718L, "RS-718", null, "Product №$718", 3854.0103909811m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 690L, "RS-690", null, "Product №$690", 5088.57243465659m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 691L, "RS-691", null, "Product №$691", 7241.59369582385m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 692L, "RS-692", null, "Product №$692", 4616.61790712579m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 693L, "RS-693", null, "Product №$693", 4392.21024252112m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 694L, "RS-694", null, "Product №$694", 7243.68992133238m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 695L, "RS-695", null, "Product №$695", 7203.25640738162m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 696L, "RS-696", null, "Product №$696", 827.622912278223m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 697L, "RS-697", null, "Product №$697", 7866.74105928593m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 698L, "RS-698", null, "Product №$698", 7482.9484976283m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 699L, "RS-699", null, "Product №$699", 4283.88320574718m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 700L, "RS-700", null, "Product №$700", 415.694085143364m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 701L, "RS-701", null, "Product №$701", 9463.20449442752m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 702L, "RS-702", null, "Product №$702", 9774.08279188633m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 627L, "RS-627", null, "Product №$627", 7171.93371950273m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 703L, "RS-703", null, "Product №$703", 5441.99289541784m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 705L, "RS-705", null, "Product №$705", 2887.65144668876m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 706L, "RS-706", null, "Product №$706", 2256.05413422736m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 707L, "RS-707", null, "Product №$707", 7516.70803293432m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 708L, "RS-708", null, "Product №$708", 2508.05122428949m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 709L, "RS-709", null, "Product №$709", 571.394316186846m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 710L, "RS-710", null, "Product №$710", 3652.67152602443m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 711L, "RS-711", null, "Product №$711", 1336.09752232958m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 712L, "RS-712", null, "Product №$712", 2382.11506157281m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 713L, "RS-713", null, "Product №$713", 3419.63574449515m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 714L, "RS-714", null, "Product №$714", 3120.88627513539m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 715L, "RS-715", null, "Product №$715", 1094.60017229179m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 716L, "RS-716", null, "Product №$716", 2894.97271314961m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 717L, "RS-717", null, "Product №$717", 7909.52388099838m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 704L, "RS-704", null, "Product №$704", 6203.97366406581m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 749L, "RS-749", null, "Product №$749", 6149.08974904059m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 626L, "RS-626", null, "Product №$626", 9198.71604498416m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 624L, "RS-624", null, "Product №$624", 6097.00735942321m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 534L, "RS-534", null, "Product №$534", 7518.38684897795m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 535L, "RS-535", null, "Product №$535", 6262.18446822008m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 536L, "RS-536", null, "Product №$536", 4342.51826924389m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 537L, "RS-537", null, "Product №$537", 1698.4649336424m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 538L, "RS-538", null, "Product №$538", 1439.98868830502m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 539L, "RS-539", null, "Product №$539", 8143.66720064714m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 540L, "RS-540", null, "Product №$540", 142.585625938413m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 541L, "RS-541", null, "Product №$541", 7214.81785048489m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 542L, "RS-542", null, "Product №$542", 3713.2510653293m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 543L, "RS-543", null, "Product №$543", 2062.82425302212m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 544L, "RS-544", null, "Product №$544", 1509.60910204314m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 545L, "RS-545", null, "Product №$545", 9075.36609986581m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 546L, "RS-546", null, "Product №$546", 4614.28399878288m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 547L, "RS-547", null, "Product №$547", 1035.58067746255m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 548L, "RS-548", null, "Product №$548", 5763.97266507334m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 549L, "RS-549", null, "Product №$549", 8604.08859728094m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 550L, "RS-550", null, "Product №$550", 4400.24608485412m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 551L, "RS-551", null, "Product №$551", 1529.61831611098m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 552L, "RS-552", null, "Product №$552", 3814.3976376459m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 553L, "RS-553", null, "Product №$553", 5520.1220817492m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 554L, "RS-554", null, "Product №$554", 3450.89927476407m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 555L, "RS-555", null, "Product №$555", 4503.43404174942m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 556L, "RS-556", null, "Product №$556", 7521.95358626635m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 557L, "RS-557", null, "Product №$557", 734.977280131996m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 558L, "RS-558", null, "Product №$558", 5563.86446839378m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 559L, "RS-559", null, "Product №$559", 2178.32291134555m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 560L, "RS-560", null, "Product №$560", 6080.43228559216m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 533L, "RS-533", null, "Product №$533", 4111.70192719982m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 561L, "RS-561", null, "Product №$561", 2482.47931361314m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 532L, "RS-532", null, "Product №$532", 9550.18290763264m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 530L, "RS-530", null, "Product №$530", 1752.97864328743m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 503L, "RS-503", null, "Product №$503", 4493.59725904353m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 504L, "RS-504", null, "Product №$504", 1852.0853770208m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 505L, "RS-505", null, "Product №$505", 3326.49347992916m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 506L, "RS-506", null, "Product №$506", 8619.53137378186m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 507L, "RS-507", null, "Product №$507", 9621.55884579828m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 508L, "RS-508", null, "Product №$508", 5801.11662196048m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 509L, "RS-509", null, "Product №$509", 129.2570401585m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 510L, "RS-510", null, "Product №$510", 5248.4590631204m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 511L, "RS-511", null, "Product №$511", 3829.66019857193m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 512L, "RS-512", null, "Product №$512", 4084.52346645506m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 513L, "RS-513", null, "Product №$513", 6224.12929601228m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 514L, "RS-514", null, "Product №$514", 1099.42270959701m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 515L, "RS-515", null, "Product №$515", 4610.15561810236m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 516L, "RS-516", null, "Product №$516", 7031.78144387518m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 517L, "RS-517", null, "Product №$517", 7873.58818476721m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 518L, "RS-518", null, "Product №$518", 5551.87611167872m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 519L, "RS-519", null, "Product №$519", 988.262529013801m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 520L, "RS-520", null, "Product №$520", 5323.58730925414m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 521L, "RS-521", null, "Product №$521", 6635.58841060642m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 522L, "RS-522", null, "Product №$522", 5531.46299697992m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 523L, "RS-523", null, "Product №$523", 6741.67313926931m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 524L, "RS-524", null, "Product №$524", 8929.73279064974m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 525L, "RS-525", null, "Product №$525", 9673.76246567525m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 526L, "RS-526", null, "Product №$526", 7246.061194337m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 527L, "RS-527", null, "Product №$527", 6137.05206016873m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 528L, "RS-528", null, "Product №$528", 3009.10497690044m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 529L, "RS-529", null, "Product №$529", 579.079110445026m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 531L, "RS-531", null, "Product №$531", 2109.42400717615m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 562L, "RS-562", null, "Product №$562", 6612.45386889784m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 563L, "RS-563", null, "Product №$563", 5222.03751151545m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 564L, "RS-564", null, "Product №$564", 8376.27839687107m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 597L, "RS-597", null, "Product №$597", 8491.21355381385m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 598L, "RS-598", null, "Product №$598", 3686.54585615105m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 599L, "RS-599", null, "Product №$599", 8370.5740460989m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 600L, "RS-600", null, "Product №$600", 4795.88880892652m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 601L, "RS-601", null, "Product №$601", 4641.46245952764m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 602L, "RS-602", null, "Product №$602", 2329.83823042821m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 603L, "RS-603", null, "Product №$603", 926.734423696406m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 604L, "RS-604", null, "Product №$604", 8336.45124842248m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 605L, "RS-605", null, "Product №$605", 9066.92957462134m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 606L, "RS-606", null, "Product №$606", 5096.01881964878m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 607L, "RS-607", null, "Product №$607", 6406.18872661432m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 608L, "RS-608", null, "Product №$608", 4674.44517867381m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 609L, "RS-609", null, "Product №$609", 5342.12981599482m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 610L, "RS-610", null, "Product №$610", 1581.09669647231m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 611L, "RS-611", null, "Product №$611", 4053.31484230855m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 612L, "RS-612", null, "Product №$612", 5502.91324290583m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 613L, "RS-613", null, "Product №$613", 5709.49777760985m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 614L, "RS-614", null, "Product №$614", 7118.84444445318m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 615L, "RS-615", null, "Product №$615", 9869.95176871771m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 616L, "RS-616", null, "Product №$616", 2109.39991851775m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 617L, "RS-617", null, "Product №$617", 2207.43748927835m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 618L, "RS-618", null, "Product №$618", 9043.20448592454m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 619L, "RS-619", null, "Product №$619", 8152.91806969462m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 620L, "RS-620", null, "Product №$620", 4844.00868641399m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 621L, "RS-621", null, "Product №$621", 249.416465055857m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 622L, "RS-622", null, "Product №$622", 9312.01888681949m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 623L, "RS-623", null, "Product №$623", 5690.78963980581m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 596L, "RS-596", null, "Product №$596", 602.36398158705m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 595L, "RS-595", null, "Product №$595", 7660.10631232527m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 594L, "RS-594", null, "Product №$594", 2063.23491505498m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 593L, "RS-593", null, "Product №$593", 9261.66577695946m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 565L, "RS-565", null, "Product №$565", 3139.03505594425m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 566L, "RS-566", null, "Product №$566", 4279.4772909393m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 567L, "RS-567", null, "Product №$567", 9972.82153925524m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 568L, "RS-568", null, "Product №$568", 8705.74244703434m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 569L, "RS-569", null, "Product №$569", 4837.23824137693m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 570L, "RS-570", null, "Product №$570", 267.637348858471m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 571L, "RS-571", null, "Product №$571", 5333.31651023278m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 572L, "RS-572", null, "Product №$572", 6433.5994964622m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 573L, "RS-573", null, "Product №$573", 7408.20891103158m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 574L, "RS-574", null, "Product №$574", 845.676903075388m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 575L, "RS-575", null, "Product №$575", 8108.76945876925m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 576L, "RS-576", null, "Product №$576", 2922.33734527712m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 577L, "RS-577", null, "Product №$577", 3468.6387439578m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 625L, "RS-625", null, "Product №$625", 6660.09635974658m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 578L, "RS-578", null, "Product №$578", 5232.06403722617m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 580L, "RS-580", null, "Product №$580", 5059.47846689237m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 581L, "RS-581", null, "Product №$581", 6210.48051687446m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 582L, "RS-582", null, "Product №$582", 373.079395095389m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 583L, "RS-583", null, "Product №$583", 4405.01637961949m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 584L, "RS-584", null, "Product №$584", 6178.8330255909m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 585L, "RS-585", null, "Product №$585", 223.360327176452m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 586L, "RS-586", null, "Product №$586", 8295.02636953026m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 587L, "RS-587", null, "Product №$587", 4030.06082588344m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 588L, "RS-588", null, "Product №$588", 660.802652435751m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 589L, "RS-589", null, "Product №$589", 3014.95280722852m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 590L, "RS-590", null, "Product №$590", 8740.23088195372m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 591L, "RS-591", null, "Product №$591", 3607.5409891119m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 592L, "RS-592", null, "Product №$592", 6134.60046524862m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 579L, "RS-579", null, "Product №$579", 9854.36669078393m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 502L, "RS-502", null, "Product №$502", 7476.65041940131m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 750L, "RS-750", null, "Product №$750", 4308.28369423201m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 752L, "RS-752", null, "Product №$752", 4012.73066830483m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 909L, "RS-909", null, "Product №$909", 7119.55295275876m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 910L, "RS-910", null, "Product №$910", 5640.57559037608m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 911L, "RS-911", null, "Product №$911", 3783.78678289418m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 912L, "RS-912", null, "Product №$912", 3615.05238507644m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 913L, "RS-913", null, "Product №$913", 2378.39466537274m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 914L, "RS-914", null, "Product №$914", 1601.87923424033m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 915L, "RS-915", null, "Product №$915", 5686.9160224157m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 916L, "RS-916", null, "Product №$916", 4939.03614344962m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 917L, "RS-917", null, "Product №$917", 8418.57324746371m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 918L, "RS-918", null, "Product №$918", 2156.28625925457m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 919L, "RS-919", null, "Product №$919", 7637.02431117977m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 920L, "RS-920", null, "Product №$920", 5538.26211743907m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 921L, "RS-921", null, "Product №$921", 252.456264687915m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 922L, "RS-922", null, "Product №$922", 6885.9114529965m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 923L, "RS-923", null, "Product №$923", 9956.71289039622m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 924L, "RS-924", null, "Product №$924", 8589.74866969034m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 925L, "RS-925", null, "Product №$925", 8784.30222570165m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 926L, "RS-926", null, "Product №$926", 1718.41909723283m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 927L, "RS-927", null, "Product №$927", 6644.98577669495m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 928L, "RS-928", null, "Product №$928", 7594.44248284886m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 929L, "RS-929", null, "Product №$929", 5214.02850524244m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 930L, "RS-930", null, "Product №$930", 9099.62428226118m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 931L, "RS-931", null, "Product №$931", 6717.47699227066m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 932L, "RS-932", null, "Product №$932", 1027.56772238182m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 933L, "RS-933", null, "Product №$933", 3724.98110110172m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 934L, "RS-934", null, "Product №$934", 5839.87595319742m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 935L, "RS-935", null, "Product №$935", 6021.88937646425m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 908L, "RS-908", null, "Product №$908", 3104.04672431948m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 936L, "RS-936", null, "Product №$936", 5012.56668707941m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 907L, "RS-907", null, "Product №$907", 5224.89957754728m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 905L, "RS-905", null, "Product №$905", 9510.5614278049m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 878L, "RS-878", null, "Product №$878", 3332.91276047608m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 879L, "RS-879", null, "Product №$879", 5771.24323498981m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 880L, "RS-880", null, "Product №$880", 9721.53343247321m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 881L, "RS-881", null, "Product №$881", 7763.04382726692m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 882L, "RS-882", null, "Product №$882", 5546.90880493583m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 883L, "RS-883", null, "Product №$883", 7286.56048294462m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 884L, "RS-884", null, "Product №$884", 9210.80870982763m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 885L, "RS-885", null, "Product №$885", 9716.40750286934m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 886L, "RS-886", null, "Product №$886", 3344.67196992816m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 887L, "RS-887", null, "Product №$887", 3137.99712953064m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 888L, "RS-888", null, "Product №$888", 2423.51853401564m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 889L, "RS-889", null, "Product №$889", 7080.23676512774m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 890L, "RS-890", null, "Product №$890", 3540.44879951535m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 891L, "RS-891", null, "Product №$891", 1129.74607903964m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 892L, "RS-892", null, "Product №$892", 5082.99843644863m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 893L, "RS-893", null, "Product №$893", 3161.08855566061m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 894L, "RS-894", null, "Product №$894", 7063.64613820968m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 895L, "RS-895", null, "Product №$895", 6767.21181569957m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 896L, "RS-896", null, "Product №$896", 8403.26233226958m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 897L, "RS-897", null, "Product №$897", 9494.71679958269m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 898L, "RS-898", null, "Product №$898", 3086.01995142457m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 899L, "RS-899", null, "Product №$899", 9607.93165937436m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 900L, "RS-900", null, "Product №$900", 9931.36728179239m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 901L, "RS-901", null, "Product №$901", 3699.64405600896m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 902L, "RS-902", null, "Product №$902", 2750.47714018751m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 903L, "RS-903", null, "Product №$903", 3793.24111332802m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 904L, "RS-904", null, "Product №$904", 2000.19050948331m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 906L, "RS-906", null, "Product №$906", 540.659884242648m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 937L, "RS-937", null, "Product №$937", 1753.6676916078m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 938L, "RS-938", null, "Product №$938", 5286.36997346132m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 939L, "RS-939", null, "Product №$939", 9700.24728202273m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 972L, "RS-972", null, "Product №$972", 3132.20327400239m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 973L, "RS-973", null, "Product №$973", 2456.03897723185m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 974L, "RS-974", null, "Product №$974", 8461.27669255309m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 975L, "RS-975", null, "Product №$975", 7418.4897250582m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 976L, "RS-976", null, "Product №$976", 218.50585947675m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 977L, "RS-977", null, "Product №$977", 1581.94587173962m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 978L, "RS-978", null, "Product №$978", 8517.05171564456m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 979L, "RS-979", null, "Product №$979", 8833.08665306917m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 980L, "RS-980", null, "Product №$980", 1269.60853173845m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 981L, "RS-981", null, "Product №$981", 9013.81532615694m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 982L, "RS-982", null, "Product №$982", 5085.77645527468m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 983L, "RS-983", null, "Product №$983", 6217.71236705487m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 984L, "RS-984", null, "Product №$984", 3385.85283299249m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 985L, "RS-985", null, "Product №$985", 9114.93519745531m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 986L, "RS-986", null, "Product №$986", 9379.04645194255m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 987L, "RS-987", null, "Product №$987", 5578.57208213702m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 988L, "RS-988", null, "Product №$988", 9655.31155916644m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 989L, "RS-989", null, "Product №$989", 6160.96493609294m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 990L, "RS-990", null, "Product №$990", 9208.15677345179m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 991L, "RS-991", null, "Product №$991", 2218.80243728813m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 992L, "RS-992", null, "Product №$992", 6550.17524797012m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 993L, "RS-993", null, "Product №$993", 2070.48168967966m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 994L, "RS-994", null, "Product №$994", 1908.10495145065m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 995L, "RS-995", null, "Product №$995", 5280.07351107899m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 996L, "RS-996", null, "Product №$996", 489.315297682451m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 997L, "RS-997", null, "Product №$997", 2143.93218613413m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 998L, "RS-998", null, "Product №$998", 7284.0369107593m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 971L, "RS-971", null, "Product №$971", 3185.36845184181m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 970L, "RS-970", null, "Product №$970", 674.349335336289m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 969L, "RS-969", null, "Product №$969", 5579.98985777608m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 968L, "RS-968", null, "Product №$968", 6538.51871217532m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 940L, "RS-940", null, "Product №$940", 9175.74761862668m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 941L, "RS-941", null, "Product №$941", 8119.77239238088m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 942L, "RS-942", null, "Product №$942", 33.9504052111648m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 943L, "RS-943", null, "Product №$943", 5303.96558125688m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 944L, "RS-944", null, "Product №$944", 1439.66117475166m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 945L, "RS-945", null, "Product №$945", 9756.66201662117m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 946L, "RS-946", null, "Product №$946", 7514.6936939632m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 947L, "RS-947", null, "Product №$947", 2704.60377107589m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 948L, "RS-948", null, "Product №$948", 1559.20932142027m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 949L, "RS-949", null, "Product №$949", 1376.73011579399m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 950L, "RS-950", null, "Product №$950", 1828.17567224995m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 951L, "RS-951", null, "Product №$951", 9984.68908480587m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 952L, "RS-952", null, "Product №$952", 7338.43054032812m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 877L, "RS-877", null, "Product №$877", 4113.58767380639m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 953L, "RS-953", null, "Product №$953", 5448.9956402448m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 955L, "RS-955", null, "Product №$955", 9678.91101710448m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 956L, "RS-956", null, "Product №$956", 6813.73260301246m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 957L, "RS-957", null, "Product №$957", 2793.76424979128m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 958L, "RS-958", null, "Product №$958", 5203.49244363769m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 959L, "RS-959", null, "Product №$959", 3215.88828378166m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 960L, "RS-960", null, "Product №$960", 7792.14233057208m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 961L, "RS-961", null, "Product №$961", 3895.6741075477m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 962L, "RS-962", null, "Product №$962", 7630.45709469843m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 963L, "RS-963", null, "Product №$963", 7890.01821907704m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 964L, "RS-964", null, "Product №$964", 8019.92867049758m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 965L, "RS-965", null, "Product №$965", 8923.09859810541m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 966L, "RS-966", null, "Product №$966", 2756.21906051236m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 967L, "RS-967", null, "Product №$967", 9890.07128397472m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 954L, "RS-954", null, "Product №$954", 4069.66954193528m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 751L, "RS-751", null, "Product №$751", 2918.09903127984m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 876L, "RS-876", null, "Product №$876", 6212.19379185335m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 874L, "RS-874", null, "Product №$874", 1981.24032094201m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 784L, "RS-784", null, "Product №$784", 1514.03646055331m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 785L, "RS-785", null, "Product №$785", 9209.44909993999m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 786L, "RS-786", null, "Product №$786", 9014.32909025546m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 787L, "RS-787", null, "Product №$787", 7751.69440906108m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 788L, "RS-788", null, "Product №$788", 7856.02529899032m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 789L, "RS-789", null, "Product №$789", 601.460002642805m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 790L, "RS-790", null, "Product №$790", 4935.55824502164m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 791L, "RS-791", null, "Product №$791", 9364.46213133841m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 792L, "RS-792", null, "Product №$792", 5757.38259859261m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 793L, "RS-793", null, "Product №$793", 9491.45930795533m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 794L, "RS-794", null, "Product №$794", 9905.99834355805m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 795L, "RS-795", null, "Product №$795", 6346.032831979m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 796L, "RS-796", null, "Product №$796", 4637.30977132791m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 797L, "RS-797", null, "Product №$797", 5054.68280755667m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 798L, "RS-798", null, "Product №$798", 4319.68413028851m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 799L, "RS-799", null, "Product №$799", 6233.72224449819m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 800L, "RS-800", null, "Product №$800", 8926.77494274768m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 801L, "RS-801", null, "Product №$801", 2450.41389598065m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 802L, "RS-802", null, "Product №$802", 8771.74365277018m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 803L, "RS-803", null, "Product №$803", 7242.70181136332m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 804L, "RS-804", null, "Product №$804", 4054.02014220786m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 805L, "RS-805", null, "Product №$805", 1000.43047266101m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 806L, "RS-806", null, "Product №$806", 1956.29233585498m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 807L, "RS-807", null, "Product №$807", 4905.93302757756m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 808L, "RS-808", null, "Product №$808", 6889.88264039619m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 809L, "RS-809", null, "Product №$809", 7416.57353351664m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 810L, "RS-810", null, "Product №$810", 2110.59425590122m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 783L, "RS-783", null, "Product №$783", 8538.52136458201m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 811L, "RS-811", null, "Product №$811", 1613.73756901069m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 782L, "RS-782", null, "Product №$782", 475.338148174499m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 780L, "RS-780", null, "Product №$780", 3888.23715219658m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 753L, "RS-753", null, "Product №$753", 8758.70314368918m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 754L, "RS-754", null, "Product №$754", 2117.02502431209m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 755L, "RS-755", null, "Product №$755", 392.937231991876m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 756L, "RS-756", null, "Product №$756", 4156.70587409134m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 757L, "RS-757", null, "Product №$757", 9635.06843412066m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 758L, "RS-758", null, "Product №$758", 3829.73689298599m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 759L, "RS-759", null, "Product №$759", 7456.2578776182m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 760L, "RS-760", null, "Product №$760", 4841.21440669578m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 761L, "RS-761", null, "Product №$761", 509.392018667139m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 762L, "RS-762", null, "Product №$762", 2829.09691931172m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 763L, "RS-763", null, "Product №$763", 6685.73106950416m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 764L, "RS-764", null, "Product №$764", 8443.84618496701m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 765L, "RS-765", null, "Product №$765", 625.611767464137m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 766L, "RS-766", null, "Product №$766", 4825.69996957933m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 767L, "RS-767", null, "Product №$767", 2409.06473827039m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 768L, "RS-768", null, "Product №$768", 2425.23850986047m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 769L, "RS-769", null, "Product №$769", 4028.62215602241m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 770L, "RS-770", null, "Product №$770", 2095.06960683273m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 771L, "RS-771", null, "Product №$771", 3307.853221571m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 772L, "RS-772", null, "Product №$772", 961.806695424862m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 773L, "RS-773", null, "Product №$773", 9106.79764072727m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 774L, "RS-774", null, "Product №$774", 1868.82050329299m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 775L, "RS-775", null, "Product №$775", 4700.45149079545m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 776L, "RS-776", null, "Product №$776", 8282.34297609066m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 777L, "RS-777", null, "Product №$777", 2542.96830508065m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 778L, "RS-778", null, "Product №$778", 3279.68034580335m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 779L, "RS-779", null, "Product №$779", 7859.78109010485m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 781L, "RS-781", null, "Product №$781", 6849.45487736233m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 812L, "RS-812", null, "Product №$812", 6355.3880883173m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 813L, "RS-813", null, "Product №$813", 5969.95580288114m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 814L, "RS-814", null, "Product №$814", 3568.02072542162m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 847L, "RS-847", null, "Product №$847", 9787.42679571147m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 848L, "RS-848", null, "Product №$848", 5923.43858253371m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 849L, "RS-849", null, "Product №$849", 1914.2388142246m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 850L, "RS-850", null, "Product №$850", 6311.97896148636m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 851L, "RS-851", null, "Product №$851", 346.734216598204m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 852L, "RS-852", null, "Product №$852", 9882.98819860583m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 853L, "RS-853", null, "Product №$853", 5085.28704526149m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 854L, "RS-854", null, "Product №$854", 4622.43956728952m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 855L, "RS-855", null, "Product №$855", 1852.76938222943m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 856L, "RS-856", null, "Product №$856", 7897.37445670058m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 857L, "RS-857", null, "Product №$857", 6947.96514555252m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 858L, "RS-858", null, "Product №$858", 8149.63790036255m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 859L, "RS-859", null, "Product №$859", 1323.41266671355m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 860L, "RS-860", null, "Product №$860", 3449.95984968262m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 861L, "RS-861", null, "Product №$861", 485.944948385444m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 862L, "RS-862", null, "Product №$862", 5705.13373040833m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 863L, "RS-863", null, "Product №$863", 1367.0949690822m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 864L, "RS-864", null, "Product №$864", 7353.43181404911m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 865L, "RS-865", null, "Product №$865", 8882.93408736723m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 866L, "RS-866", null, "Product №$866", 3390.45339421856m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 867L, "RS-867", null, "Product №$867", 9309.42998701214m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 868L, "RS-868", null, "Product №$868", 7036.94965552397m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 869L, "RS-869", null, "Product №$869", 2130.19746920569m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 870L, "RS-870", null, "Product №$870", 9914.04830474129m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 871L, "RS-871", null, "Product №$871", 6801.41753368146m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 872L, "RS-872", null, "Product №$872", 9806.07433235556m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 873L, "RS-873", null, "Product №$873", 4658.08862105854m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 846L, "RS-846", null, "Product №$846", 3009.07404302111m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 845L, "RS-845", null, "Product №$845", 3321.82067601095m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 844L, "RS-844", null, "Product №$844", 8490.86574674158m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 843L, "RS-843", null, "Product №$843", 439.451765473677m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 815L, "RS-815", null, "Product №$815", 7991.75952933345m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 816L, "RS-816", null, "Product №$816", 34.0538704926399m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 817L, "RS-817", null, "Product №$817", 4290.57555472971m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 818L, "RS-818", null, "Product №$818", 5171.69460895085m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 819L, "RS-819", null, "Product №$819", 9234.39708502702m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 820L, "RS-820", null, "Product №$820", 1611.28267720867m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 821L, "RS-821", null, "Product №$821", 7074.00556051824m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 822L, "RS-822", null, "Product №$822", 4553.03943928007m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 823L, "RS-823", null, "Product №$823", 1823.77850721766m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 824L, "RS-824", null, "Product №$824", 9093.06391100076m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 825L, "RS-825", null, "Product №$825", 2730.60747549432m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 826L, "RS-826", null, "Product №$826", 7550.47062297839m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 827L, "RS-827", null, "Product №$827", 1470.34738746954m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 875L, "RS-875", null, "Product №$875", 7502.88661453076m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 828L, "RS-828", null, "Product №$828", 9200.79929716922m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 830L, "RS-830", null, "Product №$830", 63.1417194675383m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 831L, "RS-831", null, "Product №$831", 3227.66016853399m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 832L, "RS-832", null, "Product №$832", 8223.28417479214m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 833L, "RS-833", null, "Product №$833", 7045.95810130516m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 834L, "RS-834", null, "Product №$834", 8933.00614735717m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 835L, "RS-835", null, "Product №$835", 1437.82325621593m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 836L, "RS-836", null, "Product №$836", 8077.71122459216m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 837L, "RS-837", null, "Product №$837", 3232.63633681118m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 838L, "RS-838", null, "Product №$838", 4484.50122237415m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 839L, "RS-839", null, "Product №$839", 513.605987892303m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 840L, "RS-840", null, "Product №$840", 7253.15676408501m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 841L, "RS-841", null, "Product №$841", 4108.39606267791m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 842L, "RS-842", null, "Product №$842", 861.811768664891m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 829L, "RS-829", null, "Product №$829", 5522.78767131399m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 501L, "RS-501", null, "Product №$501", 3053.41658324628m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 500L, "RS-500", null, "Product №$500", 1139.02245235584m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 499L, "RS-499", null, "Product №$499", 8774.48658401821m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 158L, "RS-158", null, "Product №$158", 5234.03178212886m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 159L, "RS-159", null, "Product №$159", 2394.98630277579m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 160L, "RS-160", null, "Product №$160", 2881.12186029606m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 161L, "RS-161", null, "Product №$161", 6933.70805444834m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 162L, "RS-162", null, "Product №$162", 8637.58940186239m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 163L, "RS-163", null, "Product №$163", 9168.07961145792m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 164L, "RS-164", null, "Product №$164", 6648.79099775515m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 165L, "RS-165", null, "Product №$165", 6016.17497672149m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 166L, "RS-166", null, "Product №$166", 8112.85933857451m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 167L, "RS-167", null, "Product №$167", 8217.19103409778m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 168L, "RS-168", null, "Product №$168", 6193.04660064776m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 169L, "RS-169", null, "Product №$169", 4047.5953109784m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 170L, "RS-170", null, "Product №$170", 4536.44697765654m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 171L, "RS-171", null, "Product №$171", 7733.97074441145m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 172L, "RS-172", null, "Product №$172", 6020.51745449217m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 173L, "RS-173", null, "Product №$173", 9626.72702950739m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 174L, "RS-174", null, "Product №$174", 7037.41238780199m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 175L, "RS-175", null, "Product №$175", 4638.71097873836m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 176L, "RS-176", null, "Product №$176", 9175.68825146914m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 177L, "RS-177", null, "Product №$177", 3773.63159962633m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 178L, "RS-178", null, "Product №$178", 3781.83346417818m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 179L, "RS-179", null, "Product №$179", 1969.08984890631m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 180L, "RS-180", null, "Product №$180", 7840.2764014156m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 181L, "RS-181", null, "Product №$181", 9137.50505034696m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 182L, "RS-182", null, "Product №$182", 405.853069576366m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 183L, "RS-183", null, "Product №$183", 87.252431589762m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 184L, "RS-184", null, "Product №$184", 4603.39324297541m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 157L, "RS-157", null, "Product №$157", 3193.66372339132m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 185L, "RS-185", null, "Product №$185", 5251.62285438349m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 156L, "RS-156", null, "Product №$156", 846.906598120419m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 154L, "RS-154", null, "Product №$154", 1891.12007240351m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 127L, "RS-127", null, "Product №$127", 174.092990427321m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 128L, "RS-128", null, "Product №$128", 6969.2477895735m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 129L, "RS-129", null, "Product №$129", 7056.13471896208m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 130L, "RS-130", null, "Product №$130", 7717.28738104798m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 131L, "RS-131", null, "Product №$131", 5462.68855010285m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 132L, "RS-132", null, "Product №$132", 8710.44800556751m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 133L, "RS-133", null, "Product №$133", 9224.76754022053m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 134L, "RS-134", null, "Product №$134", 8749.81544387984m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 135L, "RS-135", null, "Product №$135", 8864.97926379786m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 136L, "RS-136", null, "Product №$136", 2826.64836981643m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 137L, "RS-137", null, "Product №$137", 3817.97152283507m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 138L, "RS-138", null, "Product №$138", 7033.39649226209m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 139L, "RS-139", null, "Product №$139", 6504.81060915851m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 140L, "RS-140", null, "Product №$140", 2655.72948970633m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 141L, "RS-141", null, "Product №$141", 8121.76896637388m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 142L, "RS-142", null, "Product №$142", 6841.88230281783m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 143L, "RS-143", null, "Product №$143", 4219.27884417552m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 144L, "RS-144", null, "Product №$144", 1972.88928179671m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 145L, "RS-145", null, "Product №$145", 7972.08783122342m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 146L, "RS-146", null, "Product №$146", 3298.76525015513m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 147L, "RS-147", null, "Product №$147", 5927.2363856096m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 148L, "RS-148", null, "Product №$148", 9768.23992085096m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 149L, "RS-149", null, "Product №$149", 6881.99535798374m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 150L, "RS-150", null, "Product №$150", 2452.74147598666m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 151L, "RS-151", null, "Product №$151", 2465.6645266645m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 152L, "RS-152", null, "Product №$152", 5812.71559270691m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 153L, "RS-153", null, "Product №$153", 3027.20647446216m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 155L, "RS-155", null, "Product №$155", 1315.00100778183m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 186L, "RS-186", null, "Product №$186", 9649.97295739594m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 187L, "RS-187", null, "Product №$187", 5683.24153110536m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 188L, "RS-188", null, "Product №$188", 7333.64746781702m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 221L, "RS-221", null, "Product №$221", 2429.61780746915m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 222L, "RS-222", null, "Product №$222", 883.543566280763m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 223L, "RS-223", null, "Product №$223", 8758.23216454975m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 224L, "RS-224", null, "Product №$224", 6029.52264530096m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 225L, "RS-225", null, "Product №$225", 4903.46233123143m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 226L, "RS-226", null, "Product №$226", 9150.03100370524m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 227L, "RS-227", null, "Product №$227", 1382.10726500587m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 228L, "RS-228", null, "Product №$228", 6003.03828064494m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 229L, "RS-229", null, "Product №$229", 1315.390952544m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 230L, "RS-230", null, "Product №$230", 5154.53141422688m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 231L, "RS-231", null, "Product №$231", 1501.88556010923m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 232L, "RS-232", null, "Product №$232", 6203.14375320596m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 233L, "RS-233", null, "Product №$233", 7825.11915910296m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 234L, "RS-234", null, "Product №$234", 2109.8613562574m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 235L, "RS-235", null, "Product №$235", 2758.70218535825m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 236L, "RS-236", null, "Product №$236", 9403.31526538512m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 237L, "RS-237", null, "Product №$237", 4685.20845970382m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 238L, "RS-238", null, "Product №$238", 7741.70405126256m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 239L, "RS-239", null, "Product №$239", 9884.6225114002m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 240L, "RS-240", null, "Product №$240", 8806.47578221116m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 241L, "RS-241", null, "Product №$241", 3463.98439419641m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 242L, "RS-242", null, "Product №$242", 9693.4474444452m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 243L, "RS-243", null, "Product №$243", 81.2383741518661m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 244L, "RS-244", null, "Product №$244", 5295.50167978532m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 245L, "RS-245", null, "Product №$245", 944.797667183354m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 246L, "RS-246", null, "Product №$246", 221.154387211965m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 247L, "RS-247", null, "Product №$247", 5318.99780748365m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 220L, "RS-220", null, "Product №$220", 6366.20201932555m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 219L, "RS-219", null, "Product №$219", 1397.16814337166m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 218L, "RS-218", null, "Product №$218", 4564.68636848251m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 217L, "RS-217", null, "Product №$217", 8550.33697027263m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 189L, "RS-189", null, "Product №$189", 7434.81443609801m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 190L, "RS-190", null, "Product №$190", 8018.07266567744m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 191L, "RS-191", null, "Product №$191", 9632.98464642511m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 192L, "RS-192", null, "Product №$192", 8583.93974070621m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 193L, "RS-193", null, "Product №$193", 4638.4101894863m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 194L, "RS-194", null, "Product №$194", 3623.68874886245m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 195L, "RS-195", null, "Product №$195", 5722.02143525799m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 196L, "RS-196", null, "Product №$196", 9484.17956451149m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 197L, "RS-197", null, "Product №$197", 7673.80269135991m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 198L, "RS-198", null, "Product №$198", 7570.48784642037m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 199L, "RS-199", null, "Product №$199", 5956.71430507522m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 200L, "RS-200", null, "Product №$200", 9859.22849264891m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 201L, "RS-201", null, "Product №$201", 5081.57421605735m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 126L, "RS-126", null, "Product №$126", 5064.74143595656m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 202L, "RS-202", null, "Product №$202", 9734.18978496184m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 204L, "RS-204", null, "Product №$204", 2345.5483803272m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 205L, "RS-205", null, "Product №$205", 4718.77073157522m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 206L, "RS-206", null, "Product №$206", 6445.14707217233m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 207L, "RS-207", null, "Product №$207", 6185.98856319952m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 208L, "RS-208", null, "Product №$208", 5989.79408666016m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 209L, "RS-209", null, "Product №$209", 7252.40909366515m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 210L, "RS-210", null, "Product №$210", 2139.31275631269m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 211L, "RS-211", null, "Product №$211", 7073.27499849409m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 212L, "RS-212", null, "Product №$212", 9411.83025921314m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 213L, "RS-213", null, "Product №$213", 3264.94193322255m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 214L, "RS-214", null, "Product №$214", 4554.70990136019m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 215L, "RS-215", null, "Product №$215", 3743.6168099491m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 216L, "RS-216", null, "Product №$216", 6527.85498487198m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 203L, "RS-203", null, "Product №$203", 5720.64460987255m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 248L, "RS-248", null, "Product №$248", 83.7002881261056m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 125L, "RS-125", null, "Product №$125", 1139.04165157072m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 123L, "RS-123", null, "Product №$123", 5754.72274597488m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 33L, "RS-33", null, "Product №$33", 6188.30145624853m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 34L, "RS-34", null, "Product №$34", 2337.55811692102m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 35L, "RS-35", null, "Product №$35", 3049.49887704546m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 36L, "RS-36", null, "Product №$36", 2059.04995652803m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 37L, "RS-37", null, "Product №$37", 1895.34748061343m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 38L, "RS-38", null, "Product №$38", 3298.18537612361m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 39L, "RS-39", null, "Product №$39", 3383.22089211234m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 40L, "RS-40", null, "Product №$40", 4549.80699557336m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 41L, "RS-41", null, "Product №$41", 9140.91686678162m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 42L, "RS-42", null, "Product №$42", 2526.46477544981m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 43L, "RS-43", null, "Product №$43", 2177.02076406079m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 44L, "RS-44", null, "Product №$44", 1140.68612043778m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 45L, "RS-45", null, "Product №$45", 6416.23874959361m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 46L, "RS-46", null, "Product №$46", 5418.08470870279m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 47L, "RS-47", null, "Product №$47", 8154.86202396213m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 48L, "RS-48", null, "Product №$48", 9911.78231775378m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 49L, "RS-49", null, "Product №$49", 5413.79343039067m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 50L, "RS-50", null, "Product №$50", 3880.70316234636m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 51L, "RS-51", null, "Product №$51", 9962.7085309302m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 52L, "RS-52", null, "Product №$52", 6821.17568646612m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 53L, "RS-53", null, "Product №$53", 7285.03279261525m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 54L, "RS-54", null, "Product №$54", 7602.94423326987m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 55L, "RS-55", null, "Product №$55", 8885.80530829998m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 56L, "RS-56", null, "Product №$56", 8254.10370168002m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 57L, "RS-57", null, "Product №$57", 1318.32613205459m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 58L, "RS-58", null, "Product №$58", 1025.24905047624m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 59L, "RS-59", null, "Product №$59", 617.370880496395m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 32L, "RS-32", null, "Product №$32", 351.129425853086m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 60L, "RS-60", null, "Product №$60", 9138.13018665562m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 31L, "RS-31", null, "Product №$31", 549.781844275902m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 29L, "RS-29", null, "Product №$29", 8331.37706775748m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 2L, "RS-2", null, "Product №$2", 6335.37981022866m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 3L, "RS-3", null, "Product №$3", 3523.87480601849m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 4L, "RS-4", null, "Product №$4", 3740.25189491932m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 5L, "RS-5", null, "Product №$5", 9068.02704980039m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 6L, "RS-6", null, "Product №$6", 9450.08655518763m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 7L, "RS-7", null, "Product №$7", 9602.55627036214m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 8L, "RS-8", null, "Product №$8", 8913.58861183449m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 9L, "RS-9", null, "Product №$9", 9182.91147760251m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 10L, "RS-10", null, "Product №$10", 7038.86794719792m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 11L, "RS-11", null, "Product №$11", 9434.79661337789m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 12L, "RS-12", null, "Product №$12", 2766.56912302904m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 13L, "RS-13", null, "Product №$13", 1544.03367151694m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 14L, "RS-14", null, "Product №$14", 7786.07173254065m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 15L, "RS-15", null, "Product №$15", 3938.81543257219m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 16L, "RS-16", null, "Product №$16", 7830.18734670718m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 17L, "RS-17", null, "Product №$17", 6153.09286217815m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 18L, "RS-18", null, "Product №$18", 4597.55938714256m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 19L, "RS-19", null, "Product №$19", 5610.62545776862m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 20L, "RS-20", null, "Product №$20", 2377.78272124835m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 21L, "RS-21", null, "Product №$21", 9933.40655692546m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 22L, "RS-22", null, "Product №$22", 3607.81712159878m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 23L, "RS-23", null, "Product №$23", 5017.05367817406m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 24L, "RS-24", null, "Product №$24", 2498.62575554225m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 25L, "RS-25", null, "Product №$25", 3122.88101442292m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 26L, "RS-26", null, "Product №$26", 9929.89686314478m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 27L, "RS-27", null, "Product №$27", 8904.96330284744m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 28L, "RS-28", null, "Product №$28", 5981.21787699928m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 30L, "RS-30", null, "Product №$30", 9726.23906551219m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 61L, "RS-61", null, "Product №$61", 545.123252340184m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 62L, "RS-62", null, "Product №$62", 3621.33839336286m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 63L, "RS-63", null, "Product №$63", 582.211544077011m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 96L, "RS-96", null, "Product №$96", 5519.57847341875m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 97L, "RS-97", null, "Product №$97", 1944.2532313728m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 98L, "RS-98", null, "Product №$98", 2720.34835197048m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 99L, "RS-99", null, "Product №$99", 4651.60001751576m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 100L, "RS-100", null, "Product №$100", 7332.5715620688m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 101L, "RS-101", null, "Product №$101", 8839.81704192228m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 102L, "RS-102", null, "Product №$102", 8948.38646936621m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 103L, "RS-103", null, "Product №$103", 5175.2094622586m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 104L, "RS-104", null, "Product №$104", 3534.02795434651m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 105L, "RS-105", null, "Product №$105", 7945.86329625261m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 106L, "RS-106", null, "Product №$106", 7107.80104487566m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 107L, "RS-107", null, "Product №$107", 5606.8371914359m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 108L, "RS-108", null, "Product №$108", 6224.21433042m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 109L, "RS-109", null, "Product №$109", 4366.07837880313m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 110L, "RS-110", null, "Product №$110", 1478.86352682433m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 111L, "RS-111", null, "Product №$111", 6823.30734414202m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 112L, "RS-112", null, "Product №$112", 7441.95857431831m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 113L, "RS-113", null, "Product №$113", 4942.8620445276m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 114L, "RS-114", null, "Product №$114", 2912.57457477626m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 115L, "RS-115", null, "Product №$115", 7363.09534747298m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 116L, "RS-116", null, "Product №$116", 1551.94226724652m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 117L, "RS-117", null, "Product №$117", 3053.91394675426m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 118L, "RS-118", null, "Product №$118", 6131.53763866589m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 119L, "RS-119", null, "Product №$119", 9693.14187750832m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 120L, "RS-120", null, "Product №$120", 2760.47994511224m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 121L, "RS-121", null, "Product №$121", 6017.57055428697m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 122L, "RS-122", null, "Product №$122", 7992.91044380186m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 95L, "RS-95", null, "Product №$95", 4004.68374323318m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 94L, "RS-94", null, "Product №$94", 4245.09070545672m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 93L, "RS-93", null, "Product №$93", 2680.81449562722m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 92L, "RS-92", null, "Product №$92", 870.098430137196m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 64L, "RS-64", null, "Product №$64", 9456.67241209032m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 65L, "RS-65", null, "Product №$65", 6489.08610292202m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 66L, "RS-66", null, "Product №$66", 9083.6671875248m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 67L, "RS-67", null, "Product №$67", 6578.26766678052m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 68L, "RS-68", null, "Product №$68", 9206.47555459592m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 69L, "RS-69", null, "Product №$69", 4736.57285549518m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 70L, "RS-70", null, "Product №$70", 1879.76547604416m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 71L, "RS-71", null, "Product №$71", 5934.83986609375m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 72L, "RS-72", null, "Product №$72", 2854.90748605454m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 73L, "RS-73", null, "Product №$73", 1214.33849503023m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 74L, "RS-74", null, "Product №$74", 1060.81846219526m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 75L, "RS-75", null, "Product №$75", 3236.86585446674m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 76L, "RS-76", null, "Product №$76", 7406.94178147565m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 124L, "RS-124", null, "Product №$124", 9941.17768012973m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 77L, "RS-77", null, "Product №$77", 1430.79635753799m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 79L, "RS-79", null, "Product №$79", 6082.38700594864m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 80L, "RS-80", null, "Product №$80", 7704.79630572013m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 81L, "RS-81", null, "Product №$81", 1775.03483918264m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 82L, "RS-82", null, "Product №$82", 8993.18098509366m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 83L, "RS-83", null, "Product №$83", 567.424446608603m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 84L, "RS-84", null, "Product №$84", 4450.67390541112m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 85L, "RS-85", null, "Product №$85", 9763.53053458199m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 86L, "RS-86", null, "Product №$86", 3728.60615780978m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 87L, "RS-87", null, "Product №$87", 3066.09663323783m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 88L, "RS-88", null, "Product №$88", 8585.35722297866m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 89L, "RS-89", null, "Product №$89", 3451.75280862104m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 90L, "RS-90", null, "Product №$90", 4795.39517536545m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 91L, "RS-91", null, "Product №$91", 740.723824473435m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 78L, "RS-78", null, "Product №$78", 3876.36755773628m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 249L, "RS-249", null, "Product №$249", 9880.07193891335m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 250L, "RS-250", null, "Product №$250", 9194.16645038601m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 251L, "RS-251", null, "Product №$251", 933.842594238856m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 409L, "RS-409", null, "Product №$409", 132.475337075291m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 410L, "RS-410", null, "Product №$410", 4841.24873990251m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 411L, "RS-411", null, "Product №$411", 3476.54704632077m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 412L, "RS-412", null, "Product №$412", 1201.06776766529m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 413L, "RS-413", null, "Product №$413", 6981.96628921757m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 414L, "RS-414", null, "Product №$414", 4628.0131044928m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 415L, "RS-415", null, "Product №$415", 4042.08937382423m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 416L, "RS-416", null, "Product №$416", 6647.01973863273m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 417L, "RS-417", null, "Product №$417", 278.866570572772m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 418L, "RS-418", null, "Product №$418", 5866.91771441461m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 419L, "RS-419", null, "Product №$419", 6666.97780446474m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 420L, "RS-420", null, "Product №$420", 1725.05389513683m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 421L, "RS-421", null, "Product №$421", 7864.00829808042m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 422L, "RS-422", null, "Product №$422", 2317.33165789271m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 423L, "RS-423", null, "Product №$423", 3820.63910077356m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 424L, "RS-424", null, "Product №$424", 5946.38785158582m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 425L, "RS-425", null, "Product №$425", 92.5619248731816m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 426L, "RS-426", null, "Product №$426", 2795.56041247936m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 427L, "RS-427", null, "Product №$427", 2520.41854081695m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 428L, "RS-428", null, "Product №$428", 3516.72937325981m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 429L, "RS-429", null, "Product №$429", 9302.4253283173m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 430L, "RS-430", null, "Product №$430", 382.095808341212m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 431L, "RS-431", null, "Product №$431", 2681.2847343652m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 432L, "RS-432", null, "Product №$432", 7326.59094842458m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 433L, "RS-433", null, "Product №$433", 3477.52347750474m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 434L, "RS-434", null, "Product №$434", 3482.22498478472m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 435L, "RS-435", null, "Product №$435", 2793.25401540532m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 408L, "RS-408", null, "Product №$408", 6324.46212057232m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 436L, "RS-436", null, "Product №$436", 5669.51944291104m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 407L, "RS-407", null, "Product №$407", 4682.39162335284m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 405L, "RS-405", null, "Product №$405", 9224.88471922692m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 378L, "RS-378", null, "Product №$378", 6488.01955696569m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 379L, "RS-379", null, "Product №$379", 8070.39010248631m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 380L, "RS-380", null, "Product №$380", 4998.8133902656m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 381L, "RS-381", null, "Product №$381", 6460.87052135769m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 382L, "RS-382", null, "Product №$382", 2969.17501975278m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 383L, "RS-383", null, "Product №$383", 7786.00482167024m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 384L, "RS-384", null, "Product №$384", 8868.45324135779m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 385L, "RS-385", null, "Product №$385", 400.133342668476m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 386L, "RS-386", null, "Product №$386", 5251.26037432405m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 387L, "RS-387", null, "Product №$387", 2712.77266215196m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 388L, "RS-388", null, "Product №$388", 6391.23947657237m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 389L, "RS-389", null, "Product №$389", 5775.69932945804m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 390L, "RS-390", null, "Product №$390", 765.525596572797m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 391L, "RS-391", null, "Product №$391", 1978.02864107212m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 392L, "RS-392", null, "Product №$392", 7958.35801305173m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 393L, "RS-393", null, "Product №$393", 956.369452623822m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 394L, "RS-394", null, "Product №$394", 4266.74468175822m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 395L, "RS-395", null, "Product №$395", 4479.28438637372m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 396L, "RS-396", null, "Product №$396", 1898.84537919371m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 397L, "RS-397", null, "Product №$397", 1711.82575249664m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 398L, "RS-398", null, "Product №$398", 4479.20106559955m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 399L, "RS-399", null, "Product №$399", 3010.49607946095m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 400L, "RS-400", null, "Product №$400", 4588.16511770159m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 401L, "RS-401", null, "Product №$401", 2205.55937486028m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 402L, "RS-402", null, "Product №$402", 791.351078446652m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 403L, "RS-403", null, "Product №$403", 827.926448931883m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 404L, "RS-404", null, "Product №$404", 507.585685936541m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 406L, "RS-406", null, "Product №$406", 5744.26217737806m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 437L, "RS-437", null, "Product №$437", 2141.2485708209m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 438L, "RS-438", null, "Product №$438", 7278.4191357337m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 439L, "RS-439", null, "Product №$439", 9643.56852213087m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 472L, "RS-472", null, "Product №$472", 3000.44743483907m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 473L, "RS-473", null, "Product №$473", 6223.34919228374m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 474L, "RS-474", null, "Product №$474", 2011.10663917433m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 475L, "RS-475", null, "Product №$475", 1156.18514416562m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 476L, "RS-476", null, "Product №$476", 1475.69775650077m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 477L, "RS-477", null, "Product №$477", 6058.56751839564m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 478L, "RS-478", null, "Product №$478", 2886.18851121803m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 479L, "RS-479", null, "Product №$479", 8657.40930133379m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 480L, "RS-480", null, "Product №$480", 9315.60105146636m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 481L, "RS-481", null, "Product №$481", 1819.1686886452m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 482L, "RS-482", null, "Product №$482", 6192.06219268593m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 483L, "RS-483", null, "Product №$483", 3292.07406532582m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 484L, "RS-484", null, "Product №$484", 1470.1606805763m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 485L, "RS-485", null, "Product №$485", 8762.11699972028m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 486L, "RS-486", null, "Product №$486", 6836.37669628317m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 487L, "RS-487", null, "Product №$487", 9514.36768728977m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 488L, "RS-488", null, "Product №$488", 2192.08129318062m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 489L, "RS-489", null, "Product №$489", 6758.06816516354m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 490L, "RS-490", null, "Product №$490", 2905.02629843775m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 491L, "RS-491", null, "Product №$491", 8698.80746523794m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 492L, "RS-492", null, "Product №$492", 7259.70997347483m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 493L, "RS-493", null, "Product №$493", 6863.39537467034m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 494L, "RS-494", null, "Product №$494", 3214.24421538331m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 495L, "RS-495", null, "Product №$495", 1432.0275287293m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 496L, "RS-496", null, "Product №$496", 9403.20650087819m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 497L, "RS-497", null, "Product №$497", 9366.27374932462m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 498L, "RS-498", null, "Product №$498", 6508.384610763m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 471L, "RS-471", null, "Product №$471", 4505.77116781183m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 470L, "RS-470", null, "Product №$470", 8372.56993091319m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 469L, "RS-469", null, "Product №$469", 1834.75908908749m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 468L, "RS-468", null, "Product №$468", 3499.74130443285m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 440L, "RS-440", null, "Product №$440", 4655.87116529042m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 441L, "RS-441", null, "Product №$441", 568.868750971215m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 442L, "RS-442", null, "Product №$442", 6388.31054157964m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 443L, "RS-443", null, "Product №$443", 6258.76413949708m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 444L, "RS-444", null, "Product №$444", 934.450589555526m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 445L, "RS-445", null, "Product №$445", 7288.97855025203m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 446L, "RS-446", null, "Product №$446", 776.960873406828m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 447L, "RS-447", null, "Product №$447", 976.391723834161m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 448L, "RS-448", null, "Product №$448", 6328.35634813102m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 449L, "RS-449", null, "Product №$449", 224.655307933993m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 450L, "RS-450", null, "Product №$450", 7832.264647741m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 451L, "RS-451", null, "Product №$451", 1619.97880862093m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 452L, "RS-452", null, "Product №$452", 5844.90803808202m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 377L, "RS-377", null, "Product №$377", 1805.79201402412m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 453L, "RS-453", null, "Product №$453", 7812.2232611348m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 455L, "RS-455", null, "Product №$455", 6724.15681962118m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 456L, "RS-456", null, "Product №$456", 9888.22771696757m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 457L, "RS-457", null, "Product №$457", 6970.7119776731m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 458L, "RS-458", null, "Product №$458", 4881.53859734607m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 459L, "RS-459", null, "Product №$459", 415.02376106336m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 460L, "RS-460", null, "Product №$460", 6429.32430674756m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 461L, "RS-461", null, "Product №$461", 3223.84363656111m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 462L, "RS-462", null, "Product №$462", 1165.66225009303m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 463L, "RS-463", null, "Product №$463", 7022.03679225502m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 464L, "RS-464", null, "Product №$464", 9750.37952873408m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 465L, "RS-465", null, "Product №$465", 2159.96400553731m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 466L, "RS-466", null, "Product №$466", 6149.95609789619m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 467L, "RS-467", null, "Product №$467", 7723.54429016055m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 454L, "RS-454", null, "Product №$454", 1285.44218432412m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 376L, "RS-376", null, "Product №$376", 4393.11048686184m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 375L, "RS-375", null, "Product №$375", 2280.94118753492m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 374L, "RS-374", null, "Product №$374", 3781.70971469102m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 284L, "RS-284", null, "Product №$284", 2121.22450215799m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 285L, "RS-285", null, "Product №$285", 4220.68881998802m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 286L, "RS-286", null, "Product №$286", 8392.76923723182m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 287L, "RS-287", null, "Product №$287", 29.824050157249m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 288L, "RS-288", null, "Product №$288", 8234.60687335329m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 289L, "RS-289", null, "Product №$289", 4680.25067107764m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 290L, "RS-290", null, "Product №$290", 8560.67153558166m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 291L, "RS-291", null, "Product №$291", 8427.35764497302m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 292L, "RS-292", null, "Product №$292", 4994.08649513223m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 293L, "RS-293", null, "Product №$293", 299.618002166794m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 294L, "RS-294", null, "Product №$294", 4315.88278353023m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 295L, "RS-295", null, "Product №$295", 3743.4359750447m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 296L, "RS-296", null, "Product №$296", 3281.03411164183m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 297L, "RS-297", null, "Product №$297", 5019.04431032904m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 298L, "RS-298", null, "Product №$298", 7983.36069471359m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 299L, "RS-299", null, "Product №$299", 4658.07448358185m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 300L, "RS-300", null, "Product №$300", 74.6664218952257m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 301L, "RS-301", null, "Product №$301", 8634.44328710178m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 302L, "RS-302", null, "Product №$302", 4163.9172305185m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 303L, "RS-303", null, "Product №$303", 8287.69257212416m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 304L, "RS-304", null, "Product №$304", 5539.77039434936m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 305L, "RS-305", null, "Product №$305", 7351.51992521785m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 306L, "RS-306", null, "Product №$306", 125.209675228787m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 307L, "RS-307", null, "Product №$307", 8429.0524657951m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 308L, "RS-308", null, "Product №$308", 3582.62734188821m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 309L, "RS-309", null, "Product №$309", 6688.29466062053m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 310L, "RS-310", null, "Product №$310", 4693.4403221558m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 283L, "RS-283", null, "Product №$283", 6122.96634173159m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 282L, "RS-282", null, "Product №$282", 1298.40697687976m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 281L, "RS-281", null, "Product №$281", 3831.03319622159m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 280L, "RS-280", null, "Product №$280", 4682.30794401947m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 252L, "RS-252", null, "Product №$252", 3109.1163228774m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 253L, "RS-253", null, "Product №$253", 6173.31970304871m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 254L, "RS-254", null, "Product №$254", 9590.51228574966m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 255L, "RS-255", null, "Product №$255", 7429.61068517976m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 256L, "RS-256", null, "Product №$256", 4198.03064977658m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 257L, "RS-257", null, "Product №$257", 975.957620412092m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 258L, "RS-258", null, "Product №$258", 9691.12196457159m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 259L, "RS-259", null, "Product №$259", 7442.08604909577m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 260L, "RS-260", null, "Product №$260", 5568.73972786997m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 261L, "RS-261", null, "Product №$261", 5063.03980716646m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 262L, "RS-262", null, "Product №$262", 182.950282554585m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 263L, "RS-263", null, "Product №$263", 4674.40313411616m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 264L, "RS-264", null, "Product №$264", 2097.87767943827m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 311L, "RS-311", null, "Product №$311", 3395.72545764769m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 265L, "RS-265", null, "Product №$265", 637.427196203464m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 267L, "RS-267", null, "Product №$267", 1586.71110011018m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 268L, "RS-268", null, "Product №$268", 1155.08057696516m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 269L, "RS-269", null, "Product №$269", 1796.00771600195m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 270L, "RS-270", null, "Product №$270", 4340.30154456398m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 271L, "RS-271", null, "Product №$271", 1842.64652516816m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 272L, "RS-272", null, "Product №$272", 808.63291901007m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 273L, "RS-273", null, "Product №$273", 4680.06385708231m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 274L, "RS-274", null, "Product №$274", 2590.6923611605m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 275L, "RS-275", null, "Product №$275", 2902.21762512914m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 276L, "RS-276", null, "Product №$276", 2736.17036302396m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 277L, "RS-277", null, "Product №$277", 802.305192128897m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 278L, "RS-278", null, "Product №$278", 3462.73048476443m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 279L, "RS-279", null, "Product №$279", 5084.72497811761m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 266L, "RS-266", null, "Product №$266", 870.131245288128m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 999L, "RS-999", null, "Product №$999", 2516.56257664625m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 312L, "RS-312", null, "Product №$312", 7513.22713564766m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 314L, "RS-314", null, "Product №$314", 2759.7781050763m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 347L, "RS-347", null, "Product №$347", 387.689508678247m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 348L, "RS-348", null, "Product №$348", 7539.8398970905m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 349L, "RS-349", null, "Product №$349", 2578.17625188184m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 350L, "RS-350", null, "Product №$350", 9978.80314475801m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 351L, "RS-351", null, "Product №$351", 9221.05017081883m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 352L, "RS-352", null, "Product №$352", 2465.86567837087m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 353L, "RS-353", null, "Product №$353", 106.17183526334m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 354L, "RS-354", null, "Product №$354", 2413.41652461021m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 355L, "RS-355", null, "Product №$355", 9234.35922676435m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 356L, "RS-356", null, "Product №$356", 5282.33906034489m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 357L, "RS-357", null, "Product №$357", 7689.08732463098m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 358L, "RS-358", null, "Product №$358", 5052.35639170388m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 359L, "RS-359", null, "Product №$359", 9626.8264947584m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 360L, "RS-360", null, "Product №$360", 502.959895181917m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 361L, "RS-361", null, "Product №$361", 9616.19475838551m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 362L, "RS-362", null, "Product №$362", 8064.87139224302m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 363L, "RS-363", null, "Product №$363", 4735.3709557724m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 364L, "RS-364", null, "Product №$364", 7067.11114713322m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 365L, "RS-365", null, "Product №$365", 6976.31426946088m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 366L, "RS-366", null, "Product №$366", 576.780960232383m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 367L, "RS-367", null, "Product №$367", 8708.57113446508m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 368L, "RS-368", null, "Product №$368", 9596.3384302316m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 369L, "RS-369", null, "Product №$369", 6711.91344815861m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 370L, "RS-370", null, "Product №$370", 2070.5905659453m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 371L, "RS-371", null, "Product №$371", 753.918425531088m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 372L, "RS-372", null, "Product №$372", 3476.78799344077m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 373L, "RS-373", null, "Product №$373", 7783.47405501803m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 346L, "RS-346", null, "Product №$346", 914.130509325364m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 345L, "RS-345", null, "Product №$345", 5164.94607793398m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 344L, "RS-344", null, "Product №$344", 9986.81034892184m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 343L, "RS-343", null, "Product №$343", 1546.31221273277m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 315L, "RS-315", null, "Product №$315", 1737.70653164839m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 316L, "RS-316", null, "Product №$316", 3764.63283028669m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 317L, "RS-317", null, "Product №$317", 4059.983940823m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 318L, "RS-318", null, "Product №$318", 2553.17863195817m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 319L, "RS-319", null, "Product №$319", 7877.18885945025m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 320L, "RS-320", null, "Product №$320", 2244.65795897164m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 321L, "RS-321", null, "Product №$321", 840.307195130879m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 322L, "RS-322", null, "Product №$322", 3352.10422675689m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 323L, "RS-323", null, "Product №$323", 6474.82990588752m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 324L, "RS-324", null, "Product №$324", 3235.33618042028m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 325L, "RS-325", null, "Product №$325", 5912.94389959096m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 326L, "RS-326", null, "Product №$326", 6848.56003003594m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 327L, "RS-327", null, "Product №$327", 509.014916843276m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 313L, "RS-313", null, "Product №$313", 4606.39698645398m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 328L, "RS-328", null, "Product №$328", 364.181073552082m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 330L, "RS-330", null, "Product №$330", 9621.18351348731m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 331L, "RS-331", null, "Product №$331", 7717.12605269492m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 332L, "RS-332", null, "Product №$332", 2818.9444974153m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 333L, "RS-333", null, "Product №$333", 8804.65600118258m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 334L, "RS-334", null, "Product №$334", 5010.05855622238m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 335L, "RS-335", null, "Product №$335", 6047.86465691769m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 336L, "RS-336", null, "Product №$336", 9667.11596570309m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 337L, "RS-337", null, "Product №$337", 3010.7144047556m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 338L, "RS-338", null, "Product №$338", 583.195947382225m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 339L, "RS-339", null, "Product №$339", 4769.70457694014m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 340L, "RS-340", null, "Product №$340", 4095.47914475923m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 341L, "RS-341", null, "Product №$341", 9963.71677143672m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 342L, "RS-342", null, "Product №$342", 6447.19670826904m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 329L, "RS-329", null, "Product №$329", 8847.2563861158m, false });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Code", "ImageId", "Name", "Price", "PriceConfirmed" },
                values: new object[] { 1000L, "RS-1000", null, "Product №$1000", 7000.44295610881m, false });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DELETE FROM Products");
        }
    }
}
