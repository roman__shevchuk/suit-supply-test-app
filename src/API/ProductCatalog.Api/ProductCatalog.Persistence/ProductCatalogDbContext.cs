﻿using Microsoft.EntityFrameworkCore;
using ProductCatalog.Domain.Models;
using ProductCatalog.Persistence.Configurations;
using System;
using System.Linq;

namespace ProductCatalog.Persistence
{
    internal class ProductCatalogDbContext : DbContext
    {
        public ProductCatalogDbContext(DbContextOptions options)
            : base(options)
        {

        }

        public DbSet<ProductModel> Products { get; set; }

        public DbSet<FileModel> Files { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new ProductModelConfiguration());
            modelBuilder.ApplyConfiguration(new FileModelConfiguration());

            modelBuilder.Entity<ProductModel>()
                .HasData(GetInitialData());
        }

        private static ProductModel[] GetInitialData()
        {
            var rnd = new Random();
            return Enumerable.Range(1, 1000).Select(i => new ProductModel
            {
                Id = i,
                Code = $"RS-{i}",
                Name = $"Product №${i}",
                Price = (decimal)(rnd.NextDouble() * 10000)
            }).ToArray();
        }
    }
}
