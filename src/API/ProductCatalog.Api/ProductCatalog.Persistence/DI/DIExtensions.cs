﻿using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using ProductCatalog.Domain.Repositories;
using ProductCatalog.Persistence.Repositories;
using System;

namespace ProductCatalog.Persistence.DI
{
    public static class DIExtensions
    {
        public static void AddPersistenceLayer(this IServiceCollection services, ILoggerFactory loggerFactory, string connectionString)
        {
            services.AddDbContext<ProductCatalogDbContext>(options =>
            {
                options.UseSqlServer(connectionString);
                options.UseLoggerFactory(loggerFactory);
            });
            services.AddScoped<Lazy<ProductCatalogDbContext>>(sp =>
            {
                var context = sp.GetRequiredService<ProductCatalogDbContext>();
                return new Lazy<ProductCatalogDbContext>(context);
            });

            services.AddScoped<IProductRepository, ProductRepository>();
            services.AddScoped<Lazy<IProductRepository>>(sp =>
            {
                var repository = sp.GetRequiredService<IProductRepository>();
                return new Lazy<IProductRepository>(repository);
            });

            services.AddScoped<IFileRepository, FileRepository>();
            services.AddScoped<Lazy<IFileRepository>>(sp =>
            {
                var repository = sp.GetRequiredService<IFileRepository>();
                return new Lazy<IFileRepository>(repository);
            });
        }

        public static void UpdateProductCatalogDatabase(this IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>()
                .CreateScope())
            {
                using (var context = serviceScope.ServiceProvider.GetService<ProductCatalogDbContext>())
                {
                    context.Database.Migrate();
                }
            }
        }
    }
}
