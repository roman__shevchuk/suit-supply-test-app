﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProductCatalog.Domain.Models;

namespace ProductCatalog.Persistence.Configurations
{
    internal class ProductModelConfiguration : ModelConfiguration<ProductModel>
    {
        public override void Configure(EntityTypeBuilder<ProductModel> builder)
        {
            base.Configure(builder);

            builder.Property(x => x.Id).UseIdentityColumn();
            builder.Property(x => x.Name).IsRequired().HasMaxLength(300);
            builder.Property(x => x.Code).IsRequired().HasMaxLength(50);

            builder.HasOne(x => x.Image);

            builder.HasIndex(x => x.Code).IsUnique();
        }
    }
}
