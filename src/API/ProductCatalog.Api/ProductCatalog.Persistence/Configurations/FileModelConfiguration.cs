﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProductCatalog.Domain.Models;

namespace ProductCatalog.Persistence.Configurations
{
    internal class FileModelConfiguration : ModelConfiguration<FileModel>
    {
        public override void Configure(EntityTypeBuilder<FileModel> builder)
        {
            base.Configure(builder);

            builder.Property(x => x.Id).UseIdentityColumn();
            builder.Property(x => x.FilePath).IsRequired();
            builder.Property(x => x.OriginalFileName).IsRequired();
        }
    }
}
