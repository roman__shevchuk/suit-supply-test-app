﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProductCatalog.Domain.Models;

namespace ProductCatalog.Persistence.Configurations
{
    internal class ModelConfiguration<TEntity> : IEntityTypeConfiguration<TEntity>
        where TEntity : Model
    {
        private const string CurrentUtcTimeQuery = "GETUTCDATE()";

        public virtual void Configure(EntityTypeBuilder<TEntity> builder)
        {
            builder.Property(x => x.CreatedUtc)
                .HasDefaultValueSql(CurrentUtcTimeQuery);

            builder.Property(x => x.LastUpdatedUtc)
                .HasDefaultValueSql(CurrentUtcTimeQuery);
        }
    }
}
