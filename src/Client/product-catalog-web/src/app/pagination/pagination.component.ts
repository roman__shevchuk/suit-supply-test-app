import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnChanges,
  SimpleChanges
} from '@angular/core';
import { PaginationDto } from '../lib';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html'
})
export class PaginationComponent implements OnChanges {
  @Input() pagesToShow: number = 3;
  @Input() pagination: PaginationDto;
  @Output() currentPageChanged: EventEmitter<number> = new EventEmitter<number>();

  pages: number[] = [];

  ngOnChanges(changes: SimpleChanges) {
    if (changes.pagination) {
      this.setPage(this.pagination.currentPage, false);
    }
  }

  setPage(page: number, notify: boolean = true) {
    this.pages = this.getPages(page);
    if (notify) {
      this.currentPageChanged.emit(page);
    }
  }

  private get validPagesToShow(): number {
    return this.pagesToShow > 1 ? this.pagesToShow : 1;
  }

  private getPages(currentPage: number): number[] {
    const pagesToShow = this.validPagesToShow;
    const totalPages = this.pagination.totalPages;

    if (totalPages <= pagesToShow) {
      return this.range(1, totalPages);
    }

    const half = Math.floor(pagesToShow / 2);
    const addition = pagesToShow % 2 === 0 ? 1 : 0;

    if (currentPage - half + addition < 1) {
      return this.range(1, pagesToShow);
    }

    if (currentPage + half > totalPages) {
      return this.range(totalPages - pagesToShow + 1, pagesToShow);
    }

    const start = currentPage - half + addition;
    return this.range(start, pagesToShow);
  }

  private range(start: number, count: number): number[] {
    const range = [];
    for (let i = start; i < start + count; i++) {
      range.push(i);
    }

    return range;
  }
}
