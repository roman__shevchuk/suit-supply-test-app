import { ProductForUpdateDto } from './../dtos/product-for-update.dto';
import { ProductForCreateDto } from './../dtos/product-for-create.dto';
import { ProductResourceParametersDto } from './../dtos/product-resource-parameters.dto';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProductApiService {
  private url: string;
  private jsonPropName = 'json';
  private imagePropName = 'image';
  private readonly baseApiUrl = 'http://localhost:4444';

  constructor(
    private httpClient: HttpClient,
  ) {
    this.url = `${this.baseApiUrl}/api/v1.0/products`;
  }

  public searchProducts(search: ProductResourceParametersDto): Observable<any> {
    let params = new HttpParams();

    // tslint:disable-next-line:forin
    for (const property in search) {
      const value = search[property];
      if (value) {
        params = params.set(property, value);
      }
    }

    return this.httpClient.get<any>(this.url, {
      headers: { accept: 'application/json' },
      // tslint:disable-next-line:object-literal-shorthand
      params: params,
      observe: 'response'
    }).pipe(map(resp => {
        (resp.headers as any).lazyInit();
        return {
          products: resp.body,
          pagination: JSON.parse(resp.headers.get('X-Pagination'))
        };
    }));
  }

  public export(searchQuery: string) {
    return this.httpClient.get<any>(`${this.url}/excel?searchQuery=${searchQuery}`, {
      observe: 'response',
      responseType: 'blob' as 'json'
    });
  }

  public createProduct(product: ProductForCreateDto): Observable<any> {
    const formdata = new FormData();
    if (product.image) {
      formdata.append(
        this.imagePropName,
        product.image,
        encodeURIComponent(product.image.name)
      );
    }

    const { image, ...data } = product;
    formdata.append(this.jsonPropName, JSON.stringify(data));

    return this.httpClient.post<any>(this.url, formdata, {
      headers: { accept: 'application/json' }
    });
  }

  public updateProduct(
    productId: number,
    product: ProductForUpdateDto
  ): Observable<any> {
    const formdata = new FormData();

    if (product.image === null) {
      formdata.append(this.imagePropName, null);
    } else if (product.image) {
      formdata.append(
        this.imagePropName,
        product.image,
        encodeURIComponent(product.image.name)
      );
    }

    const { image, ...data } = product;
    formdata.append(this.jsonPropName, JSON.stringify(data));

    return this.httpClient.patch<any>(`${this.url}/${productId}`, formdata, {
      headers: { accept: 'application/json' }
    });
  }

  public deleteProduct(productId: number) {
    return this.httpClient.delete<any>(`${this.url}/${productId}`, {
      headers: { accept: 'application/json' }
    });
  }

  public confirmProductPrice(productId: number) {
    return this.httpClient.patch<any>(
      `${this.url}/${productId}/priceConfirmation`,
      { headers: { accept: 'application/json' } }
    );
  }
}
