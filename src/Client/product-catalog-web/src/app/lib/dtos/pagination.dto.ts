export interface PaginationDto {
    totalCount: number;
    pageSize: number;
    currentPage: number;
    totalPages: number;
}
