export interface ProductForUpdateDto {
    name?: string;
    code?: number;
    price?: number;
    image?: File;
}
