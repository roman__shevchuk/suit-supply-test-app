export interface ProductDto {
    id: number;
    name: string;
    code: string;
    price: number;
    priceConfirmed: boolean;
    imageUrl: string;
    createdUtc: Date;
    lastUpdatedUtc: Date;
}


