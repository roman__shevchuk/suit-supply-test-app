export interface ProductResourceParametersDto {
    pageNumber?: number;
    pageSize?: number;
    searchQuery?: string;
}
