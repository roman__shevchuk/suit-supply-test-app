export interface ProductForCreateDto {
    name: string;
    code: number;
    price: number;
    image: File;
}
