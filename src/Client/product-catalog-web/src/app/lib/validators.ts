export const validateRequired: (fieldName: string, value?: any) => string = (fieldName: string, value?: any) => {
    return value == null || value === '' ? `${fieldName} is required` : '';
};

export const validatePositive: (fieldName: string, value?: number) => string = (fieldName: string, value?: number) => {
    const c = value <= 0;
    return value <= 0 ? `${fieldName} should be greater than 0` : '';
};

export const validateMaxLength: (fieldName: string, maxLength: number, value?: string)
    => string = (fieldName: string, maxLength: number, value?: string) => {
    return value && value.length >  maxLength ? `${fieldName} should contain between 1 and ${maxLength} symbols` : '';
};
