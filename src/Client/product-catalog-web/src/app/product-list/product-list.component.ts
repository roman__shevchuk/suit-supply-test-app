import { ToastService } from './../core/toasts/toast.service';
import { Component, OnInit } from '@angular/core';
import {
  ProductApiService,
  ProductDto,
  ProductForUpdateDto,
  ProductForCreateDto,
  PaginationDto
} from '../lib';
import { ProductDetailDialogService } from '../product-detail/product-detail-dialog.service';
import { saveAs } from 'file-saver';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {
  searchQuery: string;
  products: ProductDto[];
  pagination: PaginationDto;

  private readonly pageSize = 9;

  constructor(
    private productService: ProductApiService,
    private productDetailDlgService: ProductDetailDialogService,
    private toastService: ToastService
  ) {}

  ngOnInit() {
    this.search();
  }

  search(searchQuery?: string, pageNumber: number = 1) {
    this.searchQuery = searchQuery || '';
    this.productService
      .searchProducts({
        searchQuery: this.searchQuery,
        pageSize: this.pageSize,
        pageNumber
      })
      .toPromise()
      .then(resp => {
        this.products = resp.products;
        this.pagination = resp.pagination;
      });
  }

  export() {
    this.productService
      .export(this.searchQuery)
      .toPromise()
      .then(resp => {
        saveAs(new Blob([resp.body]), 'Products.xlsx');
        this.toastService.createSuccessToast('Done', 'Product list exported');
      });
  }

  changePage(pageNumber: number) {
    if (pageNumber === this.pagination.currentPage) {
      return;
    }
    this.search(this.searchQuery, pageNumber);
  }

  create() {
    const onSave = (productChanges: ProductForCreateDto) => {
      this.productService
        .createProduct(productChanges)
        .toPromise()
        .then(() => {
          this.toastService.createSuccessToast('Done', 'Product created');
          this.search(this.searchQuery);
        });
    };
    this.productDetailDlgService.open({} as ProductDto, onSave);
  }

  update(product: ProductDto) {
    const onSave = (productChanges: ProductForUpdateDto) => {
      this.productService
        .updateProduct(product.id, productChanges)
        .toPromise()
        .then(() => {
          this.toastService.createSuccessToast('Done', 'Product updated');
          this.search(this.searchQuery);
        });
    };
    this.productDetailDlgService.open(product, onSave);
  }

  delete(productId: number) {
    this.productService
      .deleteProduct(productId)
      .toPromise()
      .then(() => {
        this.toastService.createSuccessToast('Done', 'Product removed');
        this.search(this.searchQuery);
      });
  }

  confirmPrice(product: ProductDto) {
    this.productService
      .confirmProductPrice(product.id)
      .toPromise()
      .then((updatedProduct: ProductDto) => {
        product.priceConfirmed = true;
        product.lastUpdatedUtc = updatedProduct.lastUpdatedUtc;

        this.toastService.createSuccessToast('Done', 'Product price confirmed');
      });
  }
}
