import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ToastService } from './toasts/toast.service';

@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {
  constructor(private toastService: ToastService) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(catchError(err => this.handleError(err)));
  }

  private handleError(err: any): Observable<any> {
    if (err.status === 400) {
      for (const errorMessage of this.getValidationErrorMessages(err)) {
        this.toastService.createErrorToast('Invalid input', errorMessage);
      }
    } else if (err.error) {
        this.toastService.createErrorToast('Error', err.error);
    }

    return throwError(err);
  }

  private getValidationErrorMessages(err: any): string[] {
    const errorMessages: string[] = [];
    if (err.error && err.error.errors) {
      const errors = err.error.errors;
      for (const key in errors) {
        if (errors.hasOwnProperty(key)) {
          const value = errors[key];
          for (const error of value) {
            errorMessages.push(error);
          }
        }
      }
    }
    return errorMessages;
  }
}
