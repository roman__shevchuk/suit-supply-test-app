import { Injectable } from '@angular/core';

@Injectable()
export class ToastService {
  toasts: any[] = [];

  show(header: string, body: string, className: string) {
    this.toasts.push({ header, body, class: className });
  }

  remove(toast) {
    this.toasts = this.toasts.filter(t => t != toast);
  }

  createSuccessToast(header: string, message: string) {
    this.show(header, message, 'bg-success text-light');
  }

  createErrorToast(header: string, message: string) {
    this.show(header, message, 'bg-danger text-light');
  }
}
