import { ProductForUpdateDto } from './../lib/dtos/product-for-update.dto';
import { ProductDto } from './../lib/dtos/product.dto';
import { Injectable } from '@angular/core';
import { NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { ProductDetailComponent } from './product-detail.component';

@Injectable({
    providedIn: 'root'
})
export class ProductDetailDialogService {
  constructor(private modalService: NgbModal) {}

  open(product: ProductDto, onSave: (update: ProductForUpdateDto) => void): void {
    const modalOptions: NgbModalOptions = {
      backdrop: true,
      keyboard: false,
      centered: true,
    };

    const ref = this.modalService.open(ProductDetailComponent, modalOptions);
    const instance = ref.componentInstance as ProductDetailComponent;

    instance.product = product;
    instance.onSave = onSave;
    instance.close = () => ref.close();
  }
}
