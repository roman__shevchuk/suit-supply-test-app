import {
  validateRequired,
  validatePositive,
  validateMaxLength
} from './../lib/validators';
import { Component, ViewChild, ElementRef, Input } from '@angular/core';
import { ProductDto, ProductForUpdateDto } from '../lib';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss']
})
export class ProductDetailComponent {
  productName: string;
  productCode: string;
  productPrice: number;
  productImageUrl: string;
  fileName: string;

  nameValidationMessage: string;
  codeValidationMessage: string;
  priceValidationMessage: string;

  private changes: any = {};
  private readonly fileReader = new FileReader();
  private readonly noImageUrl = './../../assets/images/empty_product.svg';

  @ViewChild('fileUpload', { static: false }) readonly fileUpload?: ElementRef;

  @Input() onSave: (update: ProductForUpdateDto) => void = () => {};
  @Input() close: () => void = () => {};

  constructor() {}

  get imageUrl(): string {
    return this.productImageUrl || this.noImageUrl;
  }

  set product(product: ProductDto) {
    this.productName = product.name;
    this.productCode = product.code;
    this.productPrice = product.price;
    this.productImageUrl = product.imageUrl;
  }

  save() {
    this.validate();
    if (
      this.nameValidationMessage ||
      this.codeValidationMessage ||
      this.priceValidationMessage
    ) {
      return;
    }

    this.onSave(this.changes);
    this.close();
  }

  uploadImage(): void {
    if (this.productImageUrl) {
      return;
    }
    this.fileUpload.nativeElement.click();
  }

  onImageChange(fileList: FileList) {
    const file: File = fileList[0];
    this.loadImage(file);

    // allows uploading the same file twice
    this.fileUpload.nativeElement.value = null;
  }

  onNameChange(newName: string) {
    this.productName = newName;
    this.changes.name = newName;
  }

  onCodeChange(newCode: string) {
    this.productCode = newCode;
    this.changes.code = newCode;
  }

  onPriceChange(newPrice: number) {
    this.productPrice = newPrice;
    this.changes.price = newPrice;
  }

  onImageRemove() {
    this.fileName = null;
    this.productImageUrl = null;
    this.changes.image = null;
  }

  private loadImage(file: File) {
    this.fileReader.readAsDataURL(file);
    this.fileReader.onload = () => {
      this.productImageUrl = this.fileReader.result as string;
      this.fileName = file.name;
      this.changes.image = file;
    };
  }

  private validateName() {
    const fieldName = 'Name';
    this.nameValidationMessage = validateRequired(fieldName, this.productName);
    if (!this.nameValidationMessage) {
      this.nameValidationMessage = validateMaxLength(
        fieldName,
        300,
        this.productName
      );
    }
  }

  private validateCode() {
    const fieldName = 'Code';
    this.codeValidationMessage = validateRequired(fieldName, this.productCode);
    if (!this.codeValidationMessage) {
      this.codeValidationMessage = validateMaxLength(
        fieldName,
        50,
        this.productCode
      );
    }
  }

  private validatePrice() {
    const fieldName = 'Price';
    this.priceValidationMessage = validateRequired(
      fieldName,
      this.productPrice
    );
    if (!this.priceValidationMessage) {
      this.priceValidationMessage = validatePositive(
        fieldName,
        this.productPrice
      );
    }
  }

  private validate() {
    this.validateName();
    this.validateCode();
    this.validatePrice();
  }
}
