# Getting Started
Application consists of two apps.
1.	ASP .NET Core Web API (.\src\API\ProductCatalog.Api\ProductCatalog.Api.sln)
2.	Angular App (.\src\Client\product-catalog-web)

You should run them both.

# Additional dependencies
Web API requires Azure storage emulator to be installed. It is used to store images.